# Raini Drupal

A Raini droplet that adds support for creating and running  recipes
with a Raini project. This includes and Lando environment service, and exposing
Lando settings and environment variables to Raini projects or other extensions.

@see Raini\Lando\Value\LandoInfoValue


## Install

This extension requires the `raini/core` package, which is the Core Raini
package.
