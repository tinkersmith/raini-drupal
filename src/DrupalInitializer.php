<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal;

use Raini\Core\Environment;
use Raini\Core\File\PathHelper;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\Generator\GeneratorTrait;
use Raini\Core\Project\Tenant;
use Raini\Core\Project\TenantGeneratorInterface;
use Raini\Core\Value\ValueResolverInterface;
use Raini\Drupal\Event\DrupalEvents;
use Raini\Drupal\Event\DrupalLocalSettingsEvent;
use Raini\Drupal\Utility\SaltGenerator;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\SettingsBuilder;
use Tinkersmith\SettingsBuilder\Php\Stmt\ConditionalGroup;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementsBlock;

/**
 * The extension generator for the Raini Drupal installation.
 *
 * Creates a base settings.local.php and prepares for other extensions to add
 * more customization for local environment settings and optimizations.
 */
class DrupalInitializer implements TenantGeneratorInterface
{
    use GeneratorTrait;

    /**
     * @param string[]                 $configExcludeModules
     * @param Environment              $env
     * @param PathHelper               $pathHelper
     * @param ValueResolverInterface   $valueResolver
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(protected array $configExcludeModules, protected Environment $env, protected PathHelper $pathHelper, protected ValueResolverInterface $valueResolver, protected EventDispatcherInterface $eventDispatcher)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getEnv(): Environment
    {
        return $this->env;
    }


    /**
     * {@inheritdoc}
     */
    public function isApplicable(Tenant $tenant, GenerateOptions $options): bool
    {
        return $tenant instanceof DrupalTenant;
    }

    /**
     * {@inheritdoc}
     *
     * @param DrupalTenant $tenant
     */
    public function runForTenant(Tenant $tenant, GenerateOptions $options, ?OutputInterface $output = null): void
    {
        if ($output) {
            $msg = 'Drupal settings local files';
            $output instanceof SymfonyStyle ? $output->section($msg) : $output->writeln("\n<info>$msg</>");
        }

        $fs = new Filesystem();
        /** @var DrupalDroplet $droplet */
        $droplet = $tenant->getDroplet();
        $docroot = $tenant->getDocroot();
        $keysDir = $tenant->normalizePath('.keys');
        $storageDir = $tenant->normalizePath('storage');
        $settingTpl = $this->getTemplatePath('settings.local.php');

        // From the settings file, the paths are relative the $docroot, and not
        // relative to the project directory like before.
        $relStorageDir = $this->pathHelper->makeRelative($storageDir, $docroot);

        /** @var DrupalSite[] $sites */
        $sites = $tenant->getSites();
        if (!$tenant->isMultiSite()) {
            $sites = ['default' => reset($sites)];
        } elseif (empty($sites['default'])) {
            // Setup a default site.
            $sites['default'] = reset($sites);
        }

        /** @var DrupalSite[] $sites */
        foreach ($sites as $name => $site) {
            if ($output) {
                $output->writeln(" - Local files for the <info>$name</> site");
            }

            $sitePath = "{$docroot}/sites/{$name}";
            $saltFile = "{$keysDir}/hash_salt.".('default' === $name ? '' : $name.'.').'secret';

            if (!file_exists($saltFile)) {
                SaltGenerator::createSaltFile($saltFile, 96, true);
            }

            // Ensure the availability of the private and public files paths.
            $dirs = [
                "{$sitePath}/files",
                "{$storageDir}/{$name}/twig",
                "{$storageDir}/{$name}/private",
                $tenant->normalizePath($site->getConfigDir()),
            ];
            foreach ($dirs as $siteDir) {
                if (!file_exists($siteDir)) {
                    $fs->mkdir($siteDir);
                }
            }

            // Configure and apply the local development settings.
            $settings = new SettingsBuilder();

            if ($droplet->useConfigSplit()) {
                $settings->assignValue("\$config['config_split.config_split.develop']['status']", true);
            }

            // Apply staged file proxy locla setting if config is available.
            if ($droplet->useStageFileProxy() && $fileProxy = array_filter($site->getFileProxyInfo())) {
                $fileProxy = $this->resolveValue($fileProxy);

                // Prefer the URI pattern over the a static value.
                if (!empty($fileProxy['origin'])) {
                    $origin = str_replace('{site}', $site->getName(), $fileProxy['origin']);
                    $settings->assignValue("\$config['stage_file_proxy.settings']['origin']", $origin);
                    unset($fileProxy['origin']);
                }

                // Ensure that we use the correct multi-site directory.
                if ($tenant->isMultiSite()) {
                    $filesDir = 'sites/'.$site->getName().'/files';
                    $settings->assignValue("\$config['stage_file_proxy.settings']['origin_dir']", $filesDir);
                }

                // Apply the stage file proxy settings.
                foreach ($fileProxy as $key => $value) {
                    $settings->assignValue("\$config['stage_file_proxy.settings']['{$key}']", $value);
                }
            }

            $settings->assignValue("\$settings['cache']['bins']['discovery_migration']", 'cache.backend.memory');
            $settings->assignArray("\$settings['cache']['bins']", [
                'render' => 'cache.backend.null',
                'page' => 'cache.backend.null',
                'dynamic_page_cache' => 'cache.backend.null',
            ], 1);

            $settings->assignValue("\$settings['file_private_path']", "{$relStorageDir}/{$name}/private");
            $settings->assignValue("\$settings['php_storage']['twig']['directory']", "{$relStorageDir}/{$name}");
            $settings->assignArray("\$settings['config_exclude_modules']", $this->configExcludeModules);

            // Build all the database settings to be output.
            $databases = [];
            foreach ($site->getDatabaseDefinitions() as $name => $dbInfo) {
                // Map the database types to Drupal supported database drivers.
                $dbType = isset($dbInfo['type']) ? $this->resolveValue($dbInfo['type']) : 'mysql';
                if (in_array($dbType, ['mariadb', 'percona'])) {
                    $dbType = 'mysql';
                }

                $databases[$name]['default'] = [
                    'driver' => $dbType,
                    'database' => $this->resolveValue($dbInfo['database']),
                    'host' => isset($dbInfo['host']) ? $this->resolveValue($dbInfo['host']) : 'mysql',
                    'port' => isset($dbInfo['port']) ? $this->resolveValue($dbInfo['port']) : '3306',
                    'username' => $this->resolveValue($dbInfo['user'] ?? null),
                    'password' => $this->resolveValue($dbInfo['pass'] ?? null),
                    'collation' => isset($dbInfo['collation']) ?  $this->resolveValue($dbInfo['collation']) : 'utf8mb4_general_ci',
                ];
            }

            // Create the trusted domain patterns from the site domains.
            $trustedDomains = [];
            $domains = $site->getDomain();
            $domains = is_array($domains) ? $domains : [$domains];
            foreach ($domains as $domain) {
                $domainPattern = str_replace(['.', '*'], ['\.', '[a-z0-9_\-.]+'], $domain);
                $trustedDomains[] = new Expression("'^{$domainPattern}\$'");
            }

            // Special local tooling settings.
            // This would be conditional docker, lando, or ddev settings.
            $tooling = new ConditionalGroup();

            // Allow extensions to alter the database and trusted domain
            // patterns or the entire SettingsBuilder values.
            $event = new DrupalLocalSettingsEvent($site, $settings, $tooling, $databases, $trustedDomains);
            $this->eventDispatcher->dispatch($event, DrupalEvents::DRUPAL_LOCAL_SETTINGS);

            // Apply the domain and database settings and write out the file.
            $settings->assignArray("\$settings['trusted_host_patterns']", $trustedDomains);
            $settings->assignArray("\$databases", $databases, 2);

            // Only add the local tooling blocks if there are any to add.
            if (!$tooling->isEmpty()) {
                $settings->addGeneratedBlock('local_tooling', new StatementsBlock($tooling), true);
            }

            $settings->writeFile($sitePath.'/settings.local.php', $settingTpl);
        }

        if ($output) {
            $output->writeln("");
        }
    }

    /**
     * Ensure configuration values are translated to the correct output format.
     *
     * Mostly this is for TaggedValue values and to ensure they are transformed
     * into an Expression() object so they can be output currently by the
     * PhpSettingsBuilder.
     *
     * @param mixed $value The value to transform into the corrected output format.
     *
     * @return mixed The transformed value.
     */
    protected function resolveValue(mixed $value): mixed
    {
        return ($value instanceof TaggedValue)
            ? $this->valueResolver->valueExpression($value)
            : $value;
    }
}
