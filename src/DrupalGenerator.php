<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal;

use Raini\Core\Environment;
use Raini\Core\Event\ComposerGenerateEvent;
use Raini\Core\Event\GeneratorEvents;
use Raini\Core\File\PathHelper;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\Generator\GeneratorTrait;
use Raini\Core\Project\Tenant;
use Raini\Core\Project\TenantGeneratorInterface;
use Raini\Core\Project\TenantManagerInterface;
use Raini\Drupal\Event\DrupalEvents;
use Raini\Drupal\Event\DrupalSiteSettingsEvent;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\SettingsBuilder;
use Tinkersmith\SettingsBuilder\Php\Stmt\Conditional;
use Tinkersmith\SettingsBuilder\Php\Stmt\ConditionalGroup;
use Tinkersmith\SettingsBuilder\Php\Stmt\ForEachStatement;
use Tinkersmith\SettingsBuilder\Php\Stmt\Statement;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementsBlock;

/**
 * The extension generator for the Raini Drupal installation.
 *
 * Creates a base settings.php and prepares for other extensions to add more
 * customizations settings and optimizations.
 *
 * Generator also applies the common Composer extras for Drupal installer paths
 * and scaffolding settings.
 */
class DrupalGenerator implements TenantGeneratorInterface, EventSubscriberInterface
{
    use GeneratorTrait;

    /**
     * @param Environment              $env
     * @param TenantManagerInterface   $tenantManager
     * @param PathHelper               $pathHelper
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(protected Environment $env, protected TenantManagerInterface $tenantManager, protected PathHelper $pathHelper, protected EventDispatcherInterface $eventDispatcher)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getEnv(): Environment
    {
        return $this->env;
    }

    /**
     * {@inheritdoc}
     */
    public function isApplicable(Tenant $tenant, GenerateOptions $options): bool
    {
        return $tenant instanceof DrupalTenant;
    }

    /**
     * {@inheritdoc}
     *
     * @param DrupalTenant $tenant
     */
    public function runForTenant(Tenant $tenant, GenerateOptions $options, ?OutputInterface $output = null): void
    {
        if ($output) {
            $msg = 'Creating Drupal settings files';
            $output instanceof SymfonyStyle ? $output->section($msg) : $output->writeln("\n<info>$msg</>");
        }

        $fs = new Filesystem();
        $docroot = $tenant->getDocroot();

        // Create extra settings folder for additional services settings files.
        // These files are normally managed by other extensions and allows them
        // to modify them or disable them separately.
        $extrasDir = "{$docroot}/sites/default/extra-settings";
        $keysDir = $tenant->normalizePath('.keys');

        $fs->mkdir($extrasDir);
        $fs->mkdir($keysDir);
        $this->createCacheSettingsFile($extrasDir, $tenant);

        // If only a single site for this tenant, set that site to use the
        // sites "default" folder, otherwise, create a sites file to map the
        // multi-site values back to the settings.
        /** @var DrupalSite[] $sites */
        $sites = $tenant->getSites();
        if ($tenant->isMultiSite()) {
            $this->createSitesFile($tenant, $sites);

            // Ensure a default site for multi-sites.
            if (empty($sites['default'])) {
                $sites['default'] = reset($sites);
            }
        } else {
            // Only a single site, just create it using the 'default' folder.
            $sites = ['default' => reset($sites)];

            if (\file_exists($docroot.'/sites/sites.php')) {
                $fs->remove($docroot.'/sites/sites.php');
            }
        }

        $template = $this->getTemplatePath('default.settings.php');

        // From the settings file, the paths are relative the $docroot, and not
        // relative to the project directory like before.
        $relKeysDir = $this->pathHelper->makeRelative($keysDir, $docroot);

        /** @var DrupalSite[] $sites */
        foreach ($sites as $name => $site) {
            if ($output) {
                $output->writeln(" - Settings for the <info>$name</info> site");
            }

            $sitePath = "{$docroot}/sites/{$name}";
            $configPath = $site->getConfigDir();
            $saltFile = "{$relKeysDir}/hash_salt.".('default' === $name ? '' : $name.'.').'secret';

            $fs->mkdir($sitePath);
            $fs->mkDir($configPath);

            $settings = new SettingsBuilder();
            $settings->assignValue("\$settings['hash_salt']", new Expression("is_readable('{$saltFile}') ? file_get_contents('{$saltFile}') : NULL"));
            $settings->assignValue("\$settings['config_sync_directory']", $this->pathHelper->makeRelative($configPath, $docroot));

            $codeBlocks = [];
            $codeBlocks['service_settings']['extra-settings'] = new ForEachStatement(
                new Expression("new \DirectoryIterator(\$app_root . '/sites/default/extra-settings') as \$file"),
                new Conditional(
                    new Expression("\$file->isFile() && str_ends_with(\$file->getFilename(), '.settings.php')"),
                    new Statement('include $file->getRealPath();')
                ),
                <<<EOT
                    /**
                     * Load service additional services.
                     *
                     * Extra settings are additional services and module configurations that should
                     * be available for all sites, but can be optionally included. Common examples
                     * would be: Memcache, Redis, and Solr.
                     */
                    EOT
            );

            // Block for detecting and applying hosting specific settings and
            // the local environments settings.
            $codeBlocks['environment_settings']['hosts'] = new ConditionalGroup();
            $codeBlocks['environment_settings']['local'] = new Conditional(
                new Expression("file_exists(\$app_root . '/' . \$site_path . '/settings.local.php')"),
                new Statement("include \$app_root . '/' . \$site_path . '/settings.local.php';"),
                <<<EOT
                    /**
                     * Load local development override configuration, if available.
                     *
                     * Create a settings.local.php file to override variables on secondary (staging,
                     * development, etc.) installations of this site.
                     *
                     * Typical uses of settings.local.php include:
                     * - Disabling caching.
                     * - Disabling JavaScript/CSS compression.
                     * - Rerouting outgoing emails.
                     * - Enabling of environment specific config splits.
                     *
                     * Keep this code block at the end of this file to take full effect.
                     */
                    EOT
            );

            // Provide other extensions to alter and add to the settings.php
            $event = new DrupalSiteSettingsEvent($site, $settings, $codeBlocks);
            $this->eventDispatcher->dispatch($event, DrupalEvents::DRUPAL_SITE_SETTINGS);

            // Add the code blocks to the settings builder so they can be
            // written to the settings file.
            foreach ($codeBlocks as $blockId => $statements) {
                $settings->addGeneratedBlock($blockId, new StatementsBlock($statements));
            }
            $settings->writeFile($sitePath.'/settings.php', $template);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        $events = [];
        $events[GeneratorEvents::PRE_COMPOSER] = ['onPreComposerGenerate'];

        return $events;
    }

    /**
     * Listen for the GeneratorEvents::PRE_COMPOSER event.
     *
     * Alters the Composer extra settings to add installer types and paths of
     * Drupal extensions, asset libraries and Drush packages. Also ensures
     * common settings for the Drupal Composer Scaffold.
     *
     * @param ComposerGenerateEvent $event
     */
    public function onPreComposerGenerate(ComposerGenerateEvent $event): void
    {
        $tenant = $event->getTenant();
        $composer = $event->getComposerBuilder();

        // Get the docroot path relative to the site base directory, to get
        // the path relative to where the composer.json file will be placed.
        $docroot = $this->pathHelper->makeRelative($tenant->getDocroot(), $tenant->getBasePath());
        $docroot = preg_replace('#^\./#', '', $docroot);
        $basePath = rtrim($docroot, '/').'/';

        $conflict = $composer['conflict'] ?? [];
        if (!isset($conflict['drupal/drupal'])) {
            $conflict['drupal/drupal'] = '*';
            $composer['conflict'] = $conflict;
        }

        // Prepare the Composer extra installer types and paths. Ensures the
        // minimum installation options for a Drupal installation.
        $extra = ($composer['extra'] ?? []) + [
            'installer-types' => [],
            'installer-paths' => [],
            'drupal-scaffold' => [],
        ];

        // Ensure Composer dependencies declared in custom packages are included
        // when determining the root project dependencies. These packages are
        // not installed by Composer, so the "wikimedia/composer-merge-plugin"
        // plugin is needed for them to be seen by the dependency calculations.
        if (empty($extra['merge-plugin'])) {
            $extra['merge-plugin'] = [
                'include' => [
                    "{$basePath}profiles/custom/*/composer.json",
                    "{$basePath}modules/custom/*/composer.json",
                    "{$basePath}themes/custom/*/composer.json",
                ],
                'recurse' => true,
                'replace' => false,
                'ignore-duplicates' => false,
            ];
        }

        $extra['installer-types'] = array_unique([...$extra['installer-types'], 'bower-asset', 'npm-asset']);
        $extra['installer-paths'] += [
            "{$basePath}core" => ['type:drupal-core'],
            "{$basePath}drush/contrib/{\$name}" => ['type:drupal-drush'],
            "{$basePath}profiles/contrib/{\$name}" => ['type:drupal-profile'],
            "{$basePath}profiles/custom/{\$name}" => ['type:drupal-custom-profile'],
            "{$basePath}modules/contrib/{\$name}" => ['type:drupal-module'],
            "{$basePath}modules/custom/{\$name}" => ['type:drupal-custom-module'],
            "{$basePath}themes/contrib/{\$name}" => ['type:drupal-theme'],
            "{$basePath}themes/custom/{\$name}" => ['type:drupal-custom-theme'],
            "{$basePath}libraries/{\$name}" => [
                'type:drupal-library',
                'type:bower-asset',
                'type:npm-asset',
            ],
            "{$basePath}libraries/ckeditor/Plugin/{\$name}" => [
                'type:drupal-ckeditor-plugin',
            ],
        ];

        $extra['drupal-scaffold'] += ['file-mapping' => []];
        $extra['drupal-scaffold']['locations'] = [
            'project-root' => './',
            'web-root' => $basePath,
        ];
        $extra['drupal-scaffold']['file-mapping'] += [
            '[web-root]/.ht.router.php' => false,
            '[web-root]/example.gitignore' => false,
            '[web-root]/INSTALL.txt' => false,
            '[web-root]/README.txt' => false,
            '[web-root]/README.md' => false,
            '[web-root]/sites/example.settings.local.php' => false,
            '[web-root]/sites/example.sites.php' => false,
            '[web-root]/sites/README.txt' => false,
            '[web-root]/modules/README.txt' => false,
            '[web-root]/profiles/README.txt' => false,
            '[web-root]/themes/README.txt' => false,
            '[web-root]/robots.txt' => false,
            '[web-root]/sites/default/services.yml' => [
                'path' => "{$basePath}core/assets/scaffold/files/default.services.yml",
                'overwrite' => false,
            ],
            '[web-root]/sites/development.services.yml' => [
                'path' => "{$basePath}core/assets/scaffold/files/development.services.yml",
                'overwrite' => false,
            ],
            '[project-root]/.editorconfig' => [
                'path' => "{$basePath}core/assets/scaffold/files/editorconfig",
                'overwrite' => false,
            ],
            '[project-root]/.gitattributes' => [
                'path' => "{$basePath}core/assets/scaffold/files/gitattributes",
                'overwrite' => false,
            ],
        ];

        // If not using the robotstxt module then include the scaffold file.
        /** @var DrupalDroplet $droplet */
        $droplet = $tenant->getDroplet();
        $extra['drupal-scaffold']['file-mapping']['[web-root]/robots.txt'] = $droplet->useRobotsTxtModule() ? false : [
            'path' => "{$basePath}core/assets/scaffold/files/robots.txt",
            'overwrite' => false,
        ];

        // Rewrite the extra values to the ComposerBuilder so changes will
        // persist when the file is written back out to the composer.json file.
        $composer['extra'] = $extra;
    }

    /**
     * Create and place the Drupal caching settings (Memcache or Redis).
     *
     * @param string       $dstPath The path to write the cache settings to.
     * @param DrupalTenant $tenant  The tenant to create the Drupal settings files for.
     */
    protected function createCacheSettingsFile(string $dstPath, DrupalTenant $tenant): void
    {
        /** @var DrupalDroplet $droplet */
        $droplet = $tenant->getDroplet();
        $allCacheTypes = $droplet::getAllowedCacheTypes();

        // If a cache type is specified, create that cache settings file.
        if ($cacheType = $droplet->getCacheType()) {
            unset($allCacheTypes[$cacheType]);

            $filename = $cacheType.'.settings.php';
            if ($tplFile = $this->getTemplatePath($filename)) {
                $settings = new SettingsBuilder();
                $cacheInfo = $droplet->getCacheInfo($tenant);

                // If there is a computed cache prefix, apply it to cache
                // backend. This is important for multi-tenant or multi-site
                // projects, so the caches don't overwrite each other.
                $cachePrefix = $this->tenantManager->isMultiTenant() ? $tenant->getName() : '';
                if ($tenant->isMultiSite()) {
                    $cachePrefix .= $cachePrefix ? ':' : '';
                    $cachePrefix = new Expression("'$cachePrefix' . basename(\$site_path)");
                }

                switch ($cacheType) {
                    case 'memcache':
                        $servers = $cacheInfo['servers'] ?? [];
                        $servers = array_merge(["{$cacheInfo['host']}:{$cacheInfo['port']}" => 'default'], $servers);
                        $settings->assignArray("\$settings['memcache']['servers']", $servers, 0);

                        if ($cachePrefix) {
                            $settings->assignValue("\$settings['memcache']['key_prefix']", $cachePrefix);
                        }
                        break;

                    case 'redis':
                        $settings->assignArray("\$settings['redis.connection']", $cacheInfo, 1);

                        if ($cachePrefix) {
                            $settings->assignValue("\$settings['redis.connection']['cache_prefix']", $cachePrefix);
                        }
                        break;
                }

                $settings->writeFile($dstPath.'/'.$filename, $tplFile);
            }
        }

        // Clean up all other cache settings files if they exists.
        foreach ($allCacheTypes as $cacheType) {
            $settingsFile = "{$dstPath}/{$cacheType}.settings.php";

            if (file_exists($settingsFile)) {
                unlink($settingsFile);
            }
        }
    }

    /**
     * @param DrupalTenant $tenant The Drupal tenant that the sites file is being created for.
     * @param DrupalSite[] $sites  The lists of sites to creates the sites mapping for.
     */
    protected function createSitesFile(DrupalTenant $tenant, array $sites): void
    {
        $values = [];
        foreach ($sites as $site) {
            $domains = $site->getDomain();

            foreach (is_array($domains) ? $domains : [$domains] as $domain) {
                // @todo determine if the wildcard and port number are mutually
                // exclusive? As in if we have a wildcard, placing the port in
                // front will prevent the domain from matchining sub-domains?
                $key = preg_replace('/^[\*\.]+/i', '', $domain, -1, $count);
                $parts = explode(':', $key);

                // Domain has a port number, the sites.php file has the
                // convention to move this to the front of the key.
                //
                // @see https://api.drupal.org/api/drupal/core%21assets%21scaffold%21files%21example.sites.php/10
                if (!empty($parts[1])) {
                    $key = $parts[1].'.'.$parts[0];
                }

                $values[$key] = $site->getName();
            }
        }

        if ($values) {
            $sitesPath = $tenant->getDocroot().'/sites/sites.php';
            $builder = new SettingsBuilder();
            $builder->assignArray('$sites', $values, 1);
            $builder->writeFile($sitesPath, $this->getTemplatePath('sites.php'));
        }
    }
}
