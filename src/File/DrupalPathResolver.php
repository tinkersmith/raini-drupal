<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\File;

use Raini\Core\File\PathInfo;
use Raini\Core\File\PathResolverInterface;
use Raini\Core\Project\Tenant;
use Raini\Drupal\DrupalTenant;
use Raini\Drupal\Utility\DrupalPackageLocator;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Path resolver for matching Drupal modules, themes or profiles paths.
 */
class DrupalPathResolver implements PathResolverInterface
{

    /**
     * @param Filesystem $fs
     */
    public function __construct(protected Filesystem $fs)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(string $path, string $type, Tenant $tenant): ?PathInfo
    {
        switch ($type) {
            case PathInfo::PROJECT_PATH:
                return $this->resolveProjectPath($path, $tenant);

            case 'module':
            case 'theme':
            case 'profile':
                $dirs = explode('/', $path);
                if (in_array($dirs[0], ['contrib', 'custom'])) {
                    return new PathInfo($type, "{$type}s/{$path}", $tenant->getDocroot());
                }

                // Find the resource in the active Drupal tenant.
                $pathLocator = new DrupalPackageLocator($tenant);
                if ($resource = $pathLocator->find($type, $path)) {
                    return new PathInfo($type, $resource);
                }
        }

        return null;
    }

    /**
     * Find the Drupal based path type for a project path.
     *
     * Determines the path type, and builds a PathInfo object for the path. NULL
     * is returned if the path is not a Drupal related path handled by this
     * resolver.
     *
     * @param string $path   The path, relative to the project, to resolve.
     * @param Tenant $tenant The Drupal tenant to resolve the path for.
     *
     * @return PathInfo|null The path info if the $path is a recognized Drupal path for the currently active
     *                       Drupal tenant.
     */
    public function resolveProjectPath(string $path, Tenant $tenant): ?PathInfo
    {
        if ($tenant instanceof DrupalTenant) {
            $docroot = $tenant->getDocroot();
            $docroot = preg_replace('#^\./|/+$#', '', $docroot);

            // First determine is in the active Drupal tenant's docroot.
            if (preg_match('#^(?:\./)?('.$docroot.')(?:/(.+))?$#i', $path, $matches)) {
                $type = 'drupal';
                $basePath = $matches[1];
                $relPath = $matches[2] ?? '';

                if (!empty($relPath)) {
                    $pattern = '';
                    $extensionRegex = '(module|theme)s(?:/(?:custom|contrib))?';

                    // Check if path is in the Drupal profiles path. Note that
                    // profiles can have modules or themes in a subdirectory, so
                    // if we find a matching module or theme in the subdirectory,
                    // this will get flagged as a module or theme path instead.
                    if (preg_match("#^(profiles(?:/(?:custom|contrib))?)(?:/(.+))$#", $relPath, $matches)) {
                        $type = 'profile';
                        $basePath .= '/'.$matches[1];
                        $relPath = $matches[2];
                        $pattern = '^(\w+/'.$extensionRegex.')';
                    } else {
                        $pattern = '^((?:sites/\w+/)?'.$extensionRegex.')';
                    }

                    // Check if the path is in a module or theme path. This can be
                    // in the site docroot, in a profile, or in the sites directory.
                    if (preg_match('#'.$pattern.'(?:/(.+))$#', $relPath, $matches)) {
                        $type = $matches[2];
                        $basePath .= '/'.$matches[1];
                        $relPath = $matches[3];
                    }
                }

                return new PathInfo($type, $relPath, $basePath);
            }
        }

        return null;
    }
}
