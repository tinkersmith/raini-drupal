<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal;

use Raini\Core\Project\DatabaseProviderInterface;
use Raini\Core\Project\SiteInterface;
use Raini\Core\Project\Tenant;

/**
 * Definition for a single Drupal site.
 *
 * For Drupal multi-site implementations a tenant will maintain several of
 * these definitions.
 */
class DrupalSite implements SiteInterface, DatabaseProviderInterface
{

    /**
     * The site domains.
     *
     * @var string|string[]
     */
    protected string|array $domain;

    /**
     * The path to the Drupal config sync directory, relative to the tenant.
     *
     * @var string
     */
    protected string $configDir;

    /**
     * Information and definitions for the site databases.
     *
     * @var array<string, string[]>
     */
    protected array $databases;

    /**
     * Staged File Proxy settings to apply to the site local settings.
     *
     * @var mixed[]
     */
    protected array $fileProxy;

    /**
     * @param Tenant  $tenant The tenant that this site belongs to.
     * @param string  $name   The site name (identifier)
     * @param mixed[] $values The site definition and properties.
     */
    public function __construct(protected Tenant $tenant, public readonly string $name, array $values)
    {
        $this->domain = $values['domain'] ?? '*';
        $this->databases = $values['databases'] ?? [];
        $this->fileProxy = $values['fileProxy'] ?? [];

        // Config dir is set relative to the base project directory, if it
        // was not provided, then default to a common value.
        $this->configDir = (@$values['configDir'])
            ? $tenant->normalizePath($values['configDir'], $tenant->getBasePath())
            : $tenant->getBasePath().'config/common';
    }

    /**
     * {@inheritdoc}
     */
    public function getTenant(): Tenant
    {
        return $this->tenant;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultDomain(): string
    {
        return is_array($this->domain) ? reset($this->domain) : $this->domain;
    }

    /**
     * {@inheritdoc}
     */
    public function getDomain(): string|array
    {
        return $this->domain;
    }

    /**
     * Get the path to the site config sync directory.
     *
     * The directory is relative to the tenant base path and not the Drupal
     * docroot directory. In the raini.project.yml file all the paths in the
     * tenant and site paths should be relative to the tenant location.
     *
     * @return string The path relative to the tenant base path to the site config sync directory.
     */
    public function getConfigDir(): string
    {
        return $this->configDir;
    }

    /**
     * Get file proxy settings that should be included in the settings.php.
     *
     * Normally this will be to ensure the proper origin is set, but can be used
     * to modify any of the current staged file proxy config values.
     *
     * @return mixed[] Staged file proxy configuration settings.
     */
    public function getFileProxyInfo(): array
    {
        return $this->fileProxy;
    }

    /**
     * Determine if the named database is used the the default Drupal database.
     *
     * The default Drupal database is structured for Drupal, has the caches
     * operations, and content date for a Drupal site.
     *
     * @param string $dbName The database name.
     *
     * @return bool TRUE if the identified database is the Drupal database.
     */
    public function isDrupalDatabase(string $dbName): bool
    {
        $default = $this->databases['default']['database'] ?? null;

        return 'default' === $dbName || $dbName === $default;
    }

    /**
     * @return array<string, string[]> All the database definitions directly from the site object. Used to internally
     *                                 get the databases by their data definition names.
     */
    public function getDatabaseDefinitions(): array
    {
        return $this->databases;
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabases(): array
    {
        $databases = [];
        foreach ($this->databases as $db) {
            $databases[$db['database']] = $db;
        }

        return $databases;
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabase(string $name): array
    {
        foreach ($this->databases as $key => $db) {
            if ($name === $db['database'] || $name === $key) {
                return $db;
            }
        }

        throw new \InvalidArgumentException(sprintf('No database with name: %s available for %s', $name, $this->getName()));
    }
}
