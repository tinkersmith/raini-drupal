<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Command;

use Raini\Core\Command\Option\EnvironmentTenantOption;
use Raini\Core\Command\Option\OptionCollection;
use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Project\SiteInterface;
use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrushCliTrait;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Command to run Drush for a project tenant.
 */
#[AsCommand(
    name: 'drupal:drush',
    description: 'Run Drush commands for a target Drupal site.',
    aliases: ['drush'],
)]
class DrushCommand extends Command
{

    use DrushCliTrait;

    /**
     * Collection of command options provider objects.
     *
     * @var OptionCollection
     */
    protected OptionCollection $options;

    /**
     * @param CliFactoryInterface    $cliFactory    The CLI process factory.
     * @param OptionFactoryInterface $optionFactory The command options factory.
     */
    public function __construct(protected CliFactoryInterface $cliFactory, OptionFactoryInterface $optionFactory)
    {
        $this->options = $optionFactory->createCollection([
            'tenant' => EnvironmentTenantOption::class,
        ]);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $helpText = <<<EOT
            Help tasks
            EOT;

        $this
            ->setHelp($helpText)
            ->addArgument('parameters', InputArgument::IS_ARRAY, 'The Drush command and parameters for the drush command. Use "drush list" or "drush <command> --help" if you need help with Drush.');

        $this->options->apply($this);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            @['environment' => $environment, 'site' => $site] = $this->options->getValues($input);

            /** @var EnvironmentInterface $environment */
            /** @var SiteInterface $site */
            // Only run the command if a this is a Drupal project.
            if ($site instanceof DrupalSite) {
                $drushCmd = $this->buildDrushCmd($site, $environment);
                $args = $input->getArgument('parameters');

                return $drushCmd
                    ->setTty(true)
                    ->execute($args, $output);
            }

            throw new \InvalidArgumentException('Drush commands should only be run on Drupal projects. Unable to locate Drupal tenant.');
        } catch (ProcessFailedException|CliRuntimeException $e) {
            // Drush does a good job displaying errors to the console,
            // rendering the error here would be redundant.
        } catch (\InvalidArgumentException|RuntimeException $e) {
            $this->getApplication()->renderThrowable($e, $output);
        }

        return 1;
    }
}
