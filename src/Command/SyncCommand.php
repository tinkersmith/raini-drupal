<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Command;

use Raini\Core\Command\AbstractDatabaseCommand;
use Raini\Core\Command\Option\EnvironmentOption;
use Raini\Core\Command\Option\EnvironmentTenantOption;
use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\Command\Option\TenantOption;
use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Database\DatabaseManagerInterface;
use Raini\Core\Event\Database\DatabaseEvents;
use Raini\Core\Event\Database\DbImportEvent;
use Raini\Core\Event\Database\StructureTableEvent;
use Raini\Core\Project\TenantManagerInterface;
use Raini\Core\Value\SettingsValueResolverInterface;
use Raini\Drupal\DrupalDroplet;
use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrupalTenant;
use Raini\Drupal\DrushCliTrait;
use Raini\Drupal\Event\DrupalEvents;
use Raini\Drupal\Event\DrupalSyncDBEvent;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Tinkersmith\Console\Output\StreamOutput;
use Tinkersmith\Environment\EnvironmentInterface;

/**
 * Sync command for syncing Drupal databases and files accross environments.
 */
#[AsCommand(
    name: 'drupal:sync',
    description: 'Synchronize database for a target Drupal site on another environment.',
)]
class SyncCommand extends AbstractDatabaseCommand
{

    use DrushCliTrait;

    /**
     * @param DrupalDroplet                  $droplet
     * @param TenantManagerInterface         $tenantManager
     * @param DatabaseManagerInterface       $dbManager
     * @param CliFactoryInterface            $cliFactory
     * @param SettingsValueResolverInterface $settingsResolver
     * @param EventDispatcherInterface       $eventDispatcher
     * @param OptionFactoryInterface         $optionFactory
     */
    public function __construct(protected DrupalDroplet $droplet, TenantManagerInterface $tenantManager, DatabaseManagerInterface $dbManager, CliFactoryInterface $cliFactory, SettingsValueResolverInterface $settingsResolver, EventDispatcherInterface $eventDispatcher, OptionFactoryInterface $optionFactory)
    {
        parent::__construct($tenantManager, $dbManager, $cliFactory, $settingsResolver, $eventDispatcher, $optionFactory);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        // Don't use the default site if a target site was not pointed to.
        $tenantOpt = $this->options->getHandler('tenant');
        if ($tenantOpt instanceof TenantOption) {
            $tenantOpt->useDefaultSite = false;
        }

        if ($defaultEnv = $this->droplet->getDefaultSyncEnvironment()) {
            if ($tenantOpt instanceof EnvironmentTenantOption) {
                $tenantOpt->defaultEnvironment = $defaultEnv;
            } elseif ($environment = $this->options->getHandler('environment')) {
                if ($environment instanceof EnvironmentOption) {
                    $environment->defaultEnvironment = $defaultEnv;
                }
            }
        }

        $this
            ->addOption('sanitize', null, InputOption::VALUE_NONE|InputOption::VALUE_NEGATABLE, 'Sanitize the database and exclude sensitve data like webform submissions.')
            ->addOption('deploy-scripts', null, InputOption::VALUE_NONE|InputOption::VALUE_NEGATABLE, 'Run Drupal site deploy scripts (cim, deploy:hooks, etc...) after importing the database.')
            ->addOption('from-files', null, InputOption::VALUE_NONE, 'Use local database files in the "storage/backup" folder (don\'t pull from a remote environment).');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        @['environment' => $environment, 'site' => $site, 'tenant' => $tenant] = $this->options->getValues($input);

        /** @var EnvironmentInterface $environment */
        // Get the configured Drush locations and non-default environment.
        if ('default' === $environment->id() && !$input->getOption('from-files')) {
            $output->writeln('<error>Use "--environment" or "-e" to select a target environment to sync to (dev, stage, or prod).</>');

            return 1;
        }

        $errorLevel = error_reporting();
        $styler = new SymfonyStyle($input, $output);

        try {
            // Capture the error reporting level so we don't have irrelevant
            // warnings from CLI calls.
            error_reporting(E_ALL & ~E_NOTICE);

            /** @var DrupalTenant $localTenant */
            $localTenant = $this->tenantManager->getTenant($tenant->getName());

            $fs = new Filesystem();
            $fs->mkdir('./storage/backup/');

            /** @var DrupalSite[] $sites */
            $sites = $site ? [$site] : $tenant->getSites();
            foreach ($sites as $site) {
                $styler->section('Syncing database for: '.$site->getName());

                $filename = './storage/backup/'.$site->getName().'-db_'.$environment->id().'.sql.gz';
                if (!$input->getOption('from-files')) {
                    $styler->writeln('Retrieving database from source: <info>'.$filename.'</>');
                    $this->getSourceDump($site, $environment, $filename, $input->getOption('sanitize') ?? true, $output);
                } else {
                    if (!file_exists($filename)) {
                        $err = sprintf('Missing database file "%s". Check that database export files are available and try again.', $filename);
                        throw new \InvalidArgumentException($err);
                    }

                    $styler->writeln('Using existing database file: <info>'.$filename.'</>');
                }

                /** @var DrupalSite $localSite */
                $localSite = $localTenant->getSite($site->getName());
                $dbInfo = $this->getDbInfo('default', $localSite);
                $driver = $this->dbManager->getDriver($dbInfo['type']);

                // Setup and dispatch the SyncDB events so other extensions are
                // able to perform additional tasks.
                $context = $this->cliFactory->getDefaultContext();
                $syncEvent = new DrupalSyncDBEvent($environment, $site, $context->getEnvironment(), $localSite);
                $importEvent = new DbImportEvent(
                    $localSite,
                    'default',
                    $input->getOption('deploy-scripts') ?? false,
                    $this->getDbOperationOptions($tenant, $input),
                    $context,
                    $output
                );

                $this->eventDispatcher->dispatch($syncEvent, DrupalEvents::DRUPAL_PRE_SYNCDB);
                $this->eventDispatcher->dispatch($importEvent, DatabaseEvents::PRE_IMPORT);

                $styler->writeln("Dropping tables <info>{$dbInfo['database']}</>");
                $driver->dropTables($dbInfo, $context);

                $styler->writeln("Importing database to <info>{$dbInfo['database']}</>");
                try {
                    $filestream = $this->getInputStream($filename, true);
                    $driver->execute($dbInfo, $filestream, $output, $context);

                    // Allow Drupal events to respond to the database import,
                    // and content sync completion.
                    $this->eventDispatcher->dispatch($importEvent, DatabaseEvents::POST_IMPORT);
                    $this->eventDispatcher->dispatch($syncEvent, DrupalEvents::DRUPAL_POST_SYNCDB);
                } finally {
                    if (isset($filestream) && is_resource($filestream)) {
                        fclose($filestream);
                    }
                    unset($filestream);
                }
            }

            return 0;
        } catch (\Exception $e) {
            $output->writeln('<error>Some or all sites failed to synchronize databases.</>');

            return 1;
        } finally {
            error_reporting($errorLevel);
        }
    }

    /**
     * Get or create the file resource stream to use as the SQL input.
     *
     * @param string $filepath   The target filepath for the input stream.
     * @param bool   $compressed The file should be assumed to be GZIP compressed regardless of file extension.
     *
     * @return resource The filestream resource to use as the SQL command input stream.
     *
     * @throws \InvalidArgumentException
     */
    protected function getInputStream(string $filepath, bool $compressed = false): mixed
    {
        if (is_readable($filepath)) {
            try {
                if ($compressed || preg_match('#\.gz$#i', $filepath)) {
                    // PHP Compress stream doesn't actually error until you try
                    // to read data from the stream. Read a small bit of data to
                    // see if a RuntimeException is thrown.
                    $stream = fopen("compress.zlib://{$filepath}", 'r');
                    fread($stream, 10);
                    rewind($stream);
                } else {
                    $stream = fopen("{$filepath}", 'r');
                }
            } catch (\RuntimeException) {
                // Rethrow as an invalid argument because the file was not
                // a GZip (or corrupted) file.
                // Don't get it confused with a process runtime error.
                throw new \InvalidArgumentException("Unable to uncompress or read file {$filepath} for importing");
            }

            // Only return the stream if it was successfully opened as a
            // resource handle and is ready to be passed as an input stream.
            if (is_resource($stream)) {
                return $stream;
            }
        }

        throw new \InvalidArgumentException("Unable to find or read the import source file: {$filepath}");
    }

    /**
     * Get the database dump from the remote environment.
     *
     * The resulting database dump should be populated in the file specified by
     * the $filename parameter.
     *
     * @param DrupalSite           $site        The target site to fetch the remote database files from.
     * @param EnvironmentInterface $environment The information for the remote environment to get the database from.
     * @param string               $filename    The database dump file to output the database export to.
     * @param bool                 $isSanitize  Should the SQL database dump be sanitized or treated like a backup.
     * @param OutputInterface|null $output      The output to display errors and messages to.
     */
    protected function getSourceDump(DrupalSite $site, EnvironmentInterface $environment, string $filename, bool $isSanitize = true, ?OutputInterface $output = null): void
    {
        // Remote Drush command for the target environment.
        $stream = fopen($filename, 'wb');
        try {
            $targetDb = 'default';
            $args = ['--gzip'];

            // Depending on the db type, different arguments might be needed.
            $dbType = @($environment->getDefinition()['dbType'] ?: $site->getDatabase($targetDb)['type']);
            $driver = $this->dbManager->getDriver($dbType);
            if ($dumpArgs = $driver->dumpFlags()) {
                $args[] = '--extra-dump="'.implode(' ', $dumpArgs).'"';
            };

            // Make sure we respect the settings for DB tables that are set to
            // be structure only. This also saves us from transporting
            $event = new StructureTableEvent($site, $targetDb, !$isSanitize);
            $this->eventDispatcher->dispatch($event, DatabaseEvents::STRUCTURE_TABLE_REGEX);
            if ($structTables = $event->getTablesRegex()) {
                // Make the table name regex compatable with Drush params.
                $structTables = $this->convertTableRegexToDrush($structTables);
                $args[] = '--structure-tables-list='.implode(',', $structTables);
            }

            $this
                ->buildDrushCmd($site, $environment)
                ->execute([...$args, 'sql:dump'], new StreamOutput($stream, $output));
        } finally {
            // Always ensure that the file stream is closed.
            fclose($stream);
        }
    }

    /**
     * Convert the normal Drupal filter table regex into Drush be compatible.
     *
     * Drush uses "*" as a wildcard with the "structure-tables-list" but Raini
     * uses a regular expression format (more compact). Convert these to be
     * more the Drush table wildcard format when appropriate.
     *
     * @param string[] $tableRegex List of table regex filters to convert to use the Drush wildcards.
     *
     * @return string[] The list of Drush compatible database table names.
     */
    protected function convertTableRegexToDrush(array $tableRegex): array
    {
        $names = [];
        foreach ($tableRegex as $name) {
            $name = str_replace(['.+', '.*'], ['*', '*'], $name);

            if (preg_match('#^([\w_\.]+)\(([^\)]+)\)#i', $name, $matches)) {
                foreach (explode('|', $matches[2]) as $suffix) {
                    $names[] = $matches[1].$suffix;
                }
            } else {
                $names[] = $name;
            }
        }

        return $names;
    }
}
