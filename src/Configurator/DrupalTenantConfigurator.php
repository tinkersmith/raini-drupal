<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Configurator;

use Raini\Core\Configurator\SchemaBuilder;
use Tinkersmith\Configurator\Configurator;
use Tinkersmith\Configurator\ConfiguratorInterface;
use Tinkersmith\Configurator\DynamicProperty;
use Tinkersmith\Configurator\Property;
use Tinkersmith\Configurator\PropertyInterface;
use Tinkersmith\Configurator\Type\BoolType;

/**
 * The configurator for setting up Drupal tenants.
 */
class DrupalTenantConfigurator extends Configurator
{
    /**
     * @param SchemaBuilder $builder
     */
    public function __construct(protected SchemaBuilder $builder)
    {
    }

    /**
     * Build a configurator based on if it should be a multisite or not.
     *
     * @param PropertyInterface|null $property  The dynamic property that the schema is being built for.
     * @param bool                   $multisite Is the site a multisite?
     *
     * @return ConfiguratorInterface The tenant configurator to use for the $multisite value.
     */
    public function loadTenantSchema(?PropertyInterface $property = null, bool $multisite = false): ConfiguratorInterface
    {
        $builder = $this->builder;
        $schemaPath = $multisite ? 'config/multisite.schema.yml' : 'config/tenant.schema.yml';
        $schemaPath = $this->builder->extensionPath('raini-drupal', $schemaPath);

        return new Configurator($this->builder->loadDefinition($schemaPath), $builder);
    }

    /**
     * {@inheritdoc}
     */
    public function getSchema(array $values = []): array
    {
        if (!isset($this->schema)) {
            // If values for site and docroot have been provided, determine
            // which schema to use. Multisites will have a none empty value.
            if (!empty($values['sites']) || !empty($values['docroot'])) {
                $tenantConfig = $this->loadTenantSchema(null, !empty($values['sites']));

                return [
                    'tenant' => new Property('tenant', $tenantConfig, label: 'Tenant settings'),
                ];
            }

            $this->schema = [
                'multisite' => new Property('multisite', new BoolType(), null, label: 'Is this a Drupal multisite'),
                'tenant' => new DynamicProperty('tenant', 'multisite', $this->loadTenantSchema(...), label: 'Tenant settings'),
            ];
        }

        return $this->schema;
    }

    /**
     * {@inheritdoc}
     */
    public function extract(array $values): array
    {
        return parent::extract($values)['tenant'];
    }
}
