<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal;

use Raini\Core\Extension\DropletInterface;
use Raini\Core\Project\Exception\SiteNotFoundException;
use Raini\Core\Project\SiteInterface;
use Raini\Core\Project\SiteTenantInterface;
use Raini\Core\Project\Tenant;
use Raini\Core\Project\TenantDatabaseInterface;
use Raini\Drupal\Configurator\DrupalTenantConfigurator;
use Tinkersmith\Configurator\Attribute\Configurator;

/**
 * Drupal tenant definition, which extends the standard tenant.
 *
 * Drupal tenants can be defined as a multisite and have additional settings
 * for database definitions, and Drush.
 *
 * @phpstan-consistent-constructor
 */
 #[Configurator(DrupalTenantConfigurator::class)]
class DrupalTenant extends Tenant implements SiteTenantInterface, TenantDatabaseInterface
{

    /**
     * @var DrupalDroplet
     */
    protected DropletInterface $droplet;

    /**
     * @var mixed[]
     */
    protected array $sites = [];

    /**
     * @param DrupalDroplet $droplet The droplet which this tenant belongs to.
     * @param string        $name    The name ID for the tenant.
     * @param mixed[]       $data    The tenant definition data.
     */
    public function __construct(DropletInterface $droplet, string $name, array $data)
    {
        parent::__construct($droplet, $name, $data);

        if (isset($this->data['cache']) && !is_array($this->data['cache'])) {
            throw new \InvalidArgumentException('Cache definition should object that has properties for host and port, or left empty to use defaults.');
        }

        foreach ($this->normalizeSiteData($data) as $siteName => $siteDef) {
            $this->sites[$siteName] = new DrupalSite($this, $siteName, $siteDef);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function applyDbOptions(string $op, array $values, array &$options): void
    {
        if (in_array($op, ['db:import', 'drupal:sync']) && $this->droplet->useConfigSplit()) {
            $options['config-split'] = $values['config-split'] ?? true;
        } else {
            $options['config-split'] = false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isMultiSite(): bool
    {
        return count($this->sites) > 1;
    }

    /**
     * {@inheritdoc}
     */
    public function getSite(?string $name = null, bool $exception = true): ?SiteInterface
    {
        if (!isset($name)) {
            return reset($this->sites);
        }
        if (empty($this->sites[$name]) && $exception) {
            $err = sprintf("Site of $name is not available for tenant '%s'.", $this->getName());
            throw new SiteNotFoundException($err);
        }

        return $this->sites[$name] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function getSites(): array
    {
        return $this->sites;
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabases(): array
    {
        $databases = [];
        foreach ($this->sites as $site) {
            $databases += $site->getDatabases();
        }

        return $databases;
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabase(string $name): array
    {
        foreach ($this->sites as $site) {
            try {
                return $site->getDatabase($name);
            } catch (\InvalidArgumentException) {
                // skip, database was not in this site definition.
            }
        }

        throw new \InvalidArgumentException(sprintf('No database with name: %s available for %s', $name, $this->getName()));
    }

    /**
     * Gets the cache settings from the tenant configuration.
     *
     * This is the configurations directly from the tenant definition and
     * doesn't apply the defaults based on the Drupal droplet. Use the
     * DrupalDroplet::getCacheInfo() method instead to get a processed version
     * of this information.
     *
     * The settings when defined should have the "host" and "port" properties,
     * but can exclude any of these to use the default setting.
     *
     * @return array<string, string|int> The host and port settings for for the cache service.
     */
    public function getCacheInfo(): array
    {
        return $this->data['cache'] ?? [];
    }

    /**
     * {@inheritdoc}
     */
    public function createOverridden(string $environment, array $overrides): Tenant
    {
        // No overrides, just clone the current tenant.
        if (!$overrides) {
            return clone $this;
        }

        $overrides['sites'] = $this->normalizeSiteData($overrides, false);
        $overrides += $this->data;

        if (isset($this->data['cache']) && !is_array($this->data['cache'])) {
            throw new \InvalidArgumentException('Cache definition should object that has properties for host and port, or left empty to use defaults.');
        }

        // Merge the site definition, and databases.
        foreach ($this->data['sites'] as $name => $siteDef) {
            if (!isset($overrides['sites'][$name])) {
                $overrides['sites'][$name] = $siteDef;
            } else {
                $overrides['sites'][$name] += $siteDef;
                $overrides['sites'][$name]['databases'] += $siteDef['databases'];
            }
        }

        // Create a new instance with the environment overrides applied to it.
        // Maintain the original tenant, since it might be needed, or used
        // in conjunction with another environment (e.g. syncing environments).
        return new static(
            $this->droplet,
            $this->getName(),
            $overrides
        );
    }

    /**
     * Convert supported site configurations into a consisent sites format.
     *
     * @param mixed[] $data          The raw tenant definition to extract and normalize the sites data from.
     * @param bool    $applyDefaults If TRUE then apply default values for sites and databases.
     *
     * @return mixed[] The normalized site definitions.
     */
    protected function normalizeSiteData(array $data, bool $applyDefaults = true): array
    {
        $name = $this->name;

        // Apply the sites data in a consistent way.
        if (isset($data['sites'])) {
            $sites = $data['sites'];
        } elseif ($applyDefaults || !empty($data)) {
            // If this is a single site tenant, the data could have been stored
            // in a condensed format, where the site data is at the top level.
            $sites[$name] = $data;

            // These are tenant properties, and not part of the site definition.
            unset($sites[$name]['dir'], $sites[$name]['docroot']);
        } else {
            // No site data available to return.
            return [];
        }

        // Fetch database setting defaults.
        $dbDefaults = $data['dbDefaults'] ?? [];

        // Ensure that all site information is consistently formatted and
        // contains the minimal expected settings.
        foreach ($sites as $siteName => &$siteDef) {
            $names = ($siteName !== $name) ? [$name, $siteName] : [$name];

            // Apply parent proxy data.
            if (!isset($siteDef['fileProxy'])) {
                $siteDef['fileProxy'] = $data['fileProxy'] ?? null;
            }

            // Prefer "databases", but simplify the single "database" definition
            // use case which is mostly how most projects are setup.
            if (empty($siteDef['databases']) && !empty($siteDef['database'])) {
                $siteDef['databases'] = ['default' => $siteDef['database']];
                unset($siteDef['database']);
            }

            // Ensure default settings are available for each of the sites.
            $defaultName = implode('.', $names);
            $siteDef += [
                'name' => $defaultName,
                'databases' => $applyDefaults ? [
                    'default' => ['database' => implode('_', $names)],
                ] : [],
            ];

            foreach ($siteDef['databases'] as &$database) {
                $database += $dbDefaults;
            }
            unset($database);
        }
        unset($siteDef);

        return $sites;
    }
}
