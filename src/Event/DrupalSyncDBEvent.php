<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Event;

use Raini\Drupal\DrupalSite;
use Symfony\Contracts\EventDispatcher\Event;
use Tinkersmith\Environment\EnvironmentInterface;

/**
 * Event class for drupal:sync commands.
 */
class DrupalSyncDBEvent extends Event
{

    /**
     * @param EnvironmentInterface $srcEnv
     * @param DrupalSite           $srcSite
     * @param EnvironmentInterface $targetEnv
     * @param DrupalSite           $targetSite
     */
    public function __construct(protected EnvironmentInterface $srcEnv, protected DrupalSite $srcSite, protected EnvironmentInterface $targetEnv, protected DrupalSite $targetSite)
    {
    }

    /**
     * Gets the source environment for where the data sync is from.
     *
     * @return EnvironmentInterface The environment to get the source database from.
     */
    public function getSourceEnvironment(): EnvironmentInterface
    {
        return $this->srcEnv;
    }

    /**
     * Get the target environment.
     *
     * @return EnvironmentInterface The target environment where to sync the data into.
     */
    public function getTargetEnvironment(): EnvironmentInterface
    {
        return $this->targetEnv;
    }

    /**
     * Get the source site definition.
     *
     * @return DrupalSite Get the site definition of where the source database is.
     */
    public function getSourceSite(): DrupalSite
    {
        return $this->srcSite;
    }

    /**
     * Get the site definition where to sync the database into.
     *
     * @return DrupalSite The target Drupal site to import the data into.
     */
    public function getTargetSite(): DrupalSite
    {
        return $this->targetSite;
    }
}
