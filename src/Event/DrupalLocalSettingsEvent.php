<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Event;

use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrupalTenant;
use Symfony\Contracts\EventDispatcher\Event;
use Tinkersmith\SettingsBuilder\Php\SettingsBuilder;
use Tinkersmith\SettingsBuilder\Php\Stmt\ConditionalGroup;

/**
 * Event to allow extensions to alter and extend the Drupal local site settings.
 *
 * This event class is used by the DrupalInitializer before the local site
 * settings file is being written. This is for the event named
 * DrupalEvents::DRUPAL_LOCAL_SETTINGS.
 */
class DrupalLocalSettingsEvent extends Event
{

    /**
     * @param DrupalSite       $site           The Drupal site data the settings are being built for.
     * @param SettingsBuilder  $builder        The PHP settings builder being used to build the settings.
     * @param ConditionalGroup $tooling        Conditionals which apply settings based on local development tooling.
     * @param mixed[]          $databases      The generated code blocks for the settings file.
     * @param mixed[]          $trustedDomains List of trusted domain patterns.
     */
    public function __construct(protected DrupalSite $site, protected SettingsBuilder $builder, protected ConditionalGroup $tooling, protected array &$databases, protected array &$trustedDomains)
    {
    }

    /**
     * Gets the Drupal tenant of the site whose sites setting is being written.
     *
     * @return DrupalTenant The Drupal tenant that is being updated.
     */
    public function getTenant(): DrupalTenant
    {
        /** @var DrupalTenant $tenant */
        $tenant = $this->site->getTenant();

        return $tenant;
    }

    /**
     * @return DrupalSite The Drupal site information that the site settings is being written for.
     */
    public function getSite(): DrupalSite
    {
        return $this->site;
    }

    /**
     * @return SettingsBuilder The settings builder to write the site settings with.
     */
    public function getSettingsBuilder(): SettingsBuilder
    {
        return $this->builder;
    }

    /**
     * @return ConditionalGroup The conditionals for adding settings for local tooling (docker, lando, ddev, etc...).
     */
    public function getTooling(): ConditionalGroup
    {
        return $this->tooling;
    }

    /**
     * @return mixed[] The database settings for this site to be written to the local settings file.
     */
    public function &getDatabases(): array
    {
        return $this->databases;
    }

    /**
     * @return mixed[] The trusted hosts pattern. Values can be anything output by the SettingsBuilder.
     */
    public function &getTrustedDomains(): array
    {
        return $this->trustedDomains;
    }
}
