<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Event;

/**
 * Events for Drupal based projects.
 */
final class DrupalEvents
{

    /**
     * Event for altering Drupal settings file before it gets written.
     */
    const DRUPAL_SITE_SETTINGS = 'raini_drupal:generate_site_settings';

    /**
     * Event for altering local Drupal settings file as it's getting written.
     */
    const DRUPAL_LOCAL_SETTINGS = 'raini_drupal:init_local_settings';

    /**
     * Event before drupal:sync command will empty the current database content.
     *
     * This event is called after the database source is available, and the
     * current database is about to be emptied. This is a final chance to
     * capture or abort, before the database is emptied.
     */
    const DRUPAL_PRE_SYNCDB = 'raini_drupal:pre_sync';

    /**
     * Event after drupal:sync command had imported the database and run updb.
     */
    const DRUPAL_POST_SYNCDB = 'raini_drupal:post_sync';
}
