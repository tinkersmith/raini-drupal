<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Event;

use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrupalTenant;
use Symfony\Contracts\EventDispatcher\Event;
use Tinkersmith\SettingsBuilder\Php\SettingsBuilder;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementInterface;
use Tinkersmith\SettingsBuilder\Php\Stmt\ConditionalGroup;

/**
 * Event to allow extensions to alter and extend the Drupal site settings build.
 *
 * This event class is used by the DrupalGenerator when the site settings are
 * being generated, and is thrown with a DrupalEvents::DRUPAL_SITE_SETTINGS
 * event name.
 */
class DrupalSiteSettingsEvent extends Event
{

    /**
     * @param DrupalSite                          $site       The Drupal site data the settings are being built for.
     * @param SettingsBuilder                     $builder    The PHP settings builder being used to build the settings.
     * @param array<string, StatementInterface[]> $codeBlocks The generated code blocks for the settings file.
     */
    public function __construct(protected DrupalSite $site, protected SettingsBuilder $builder, protected array &$codeBlocks = [])
    {
    }

    /**
     * Gets the Drupal tenant of the site whose sites setting is being written.
     *
     * @return DrupalTenant The Drupal tenant that is being updated.
     */
    public function getTenant(): DrupalTenant
    {
        /** @var DrupalTenant $tenant */
        $tenant = $this->site->getTenant();

        return $tenant;
    }

    /**
     * @return DrupalSite The Drupal site information that the site settings is being written for.
     */
    public function getSite(): DrupalSite
    {
        return $this->site;
    }

    /**
     * @return SettingsBuilder The settings builder to write the site settings with.
     */
    public function getSettingsBuilder(): SettingsBuilder
    {
        return $this->builder;
    }

    /**
     * @return array<string, StatementInterface[]> Reference to all the generated code block definitions.
     */
    public function &getCodeBlocks(): array
    {
        return $this->codeBlocks;
    }

    /**
     * @return ConditionalGroup|null The conditional group for implementing the if/else statement for detecting and
     *                               including the settings for the hosting environments.
     */
    public function getHostingGroup(): ?ConditionalGroup
    {
        /** @var \Tinkersmith\SettingsBuilder\Php\Stmt\ConditionalGroup|null $group */
        $group = $this->codeBlocks['environment_settings']['hosts'] ?? null;

        return $group;
    }
}
