<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Test;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Environment;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Raini\Core\Test\TesterInterface;
use Raini\Drupal\DrupalTenant;
use Symfony\Component\Filesystem\Filesystem;
use Tinkersmith\Console\DockerContext;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Tester interface which runs PHPUnit tests for Drupal extensions and profiles.
 */
class DrupalTest implements TesterInterface
{

    /**
     * @param Environment         $env
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected Environment $env, protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'Drupal tester';
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceId(): string
    {
        return 'tester.drupal';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return 'Run tests using PHPUnit on Drupal modules, profiles and themes.';
    }

    /**
     * {@inheritdoc}
     */
    public function isPathApplicable(PathInfo $path, Tenant $tenant): bool
    {
        return in_array($path->getType(), [
            'drupal',
            'profile',
            'module',
            'theme',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(array|PathInfo $paths, Tenant $tenant, EnvironmentInterface|ExecutionContextInterface $context): int
    {
        /** @var DrupalTenant $tenant */
        if (!file_exists($tenant->getDocroot().'/sites/simpletest')) {
            $fs = new Filesystem();
            $fs->mkdir($tenant->getDocroot().'/sites/simpletest/browser_output', 0777);
            $fs->chmod($tenant->getDocroot().'/sites/simpletest', 0777, 0, true);
        }

        $context = $context instanceof EnvironmentInterface ? $context->createContext() : $context;
        $phpunit = $tenant->getBinDir().'/phpunit';
        if (!file_exists($phpunit)) {
            $phpunit = $this->env->getBinPath().'/phpunit';
        }

        // Apply tenant specific test configurations.
        $testConfig = $tenant->get('phpunit') ?? [];
        if (!empty($testConfig['user'])) {
            $context->runAs($testConfig['user']);
        }
        if ($context instanceof DockerContext) {
            $context->setContainer($testConfig['container'] ?? 'web');
        }

        // Setup the command environment variables to ensure configurations.
        $testUrl = @$testConfig['uri'] ?: 'http://'.($tenant->getSite()->getDefaultDomain() ?: 'localhost');
        if (!empty($testConfig['outputUri'])) {
            $testOutputUrl = $testConfig['outputUri'];
        } else {
            $testOutputUrl = 'http://'.($tenant->getSite()->getDefaultDomain() ?: 'localhost');
        }

        $cmd = $this->cliFactory->create($phpunit, $context)
            ->setTty(true)
            ->setEnv([
                'XDEBUG_MODE' => 'off',
                'SIMPLETEST_DB' => 'sqlite://localhost/sites/simpletest/test.sqlite',
                'SIMPLETEST_BASE_URL' => $testUrl,
                'BROWSERTEST_OUTPUT_BASE_URL' => $testOutputUrl,
            ]);

        // Prefer a project phpunit.xml file, but fallback to a general Drupal
        // phpunit.xml available through the Drupal droplet extension.
        $args = file_exists($tenant->getBasePath().'php.xml')
            ? ['-c', new PathInfo('project', $tenant->getBasePath().'phpunit.xml', makeAbs: true)]
            : [
                '-c', new PathInfo('project', $tenant->getDroplet()->getPath().'/phpunit.xml', makeAbs: true),
                '--bootstrap', new PathInfo('project', $tenant->getDocroot().'/core/tests/bootstrap.php', makeAbs: true),
            ];

        foreach ($paths as $path) {
            $retVal = $cmd->execute(array_merge($args, [$path]));

            // Non-zero return, should indicate and error occurred and we should
            // exit from running any additional tests.
            if ($retVal) {
                return $retVal;
            }
        }

        return 0;
    }
}
