<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal;

use Raini\Core\Configurator\ExtensionSchemaConfigurator;
use Raini\Core\Environment;
use Raini\Core\Extension\AbstractDroplet;
use Raini\Core\Extension\ComposerExtensionInterface;
use Raini\Core\Extension\ExtensionDefinitionInterface;
use Raini\Core\File\PathHelper;
use Raini\Core\Project\Tenant;
use Raini\Core\Utility\ComposerBuilder;
use Tinkersmith\Configurator\Attribute\Configurator;

/**
 * The Drupal Droplet extension handler.
 */
#[Configurator(ExtensionSchemaConfigurator::class, 'raini-drupal', 'config/droplet.schema.yml')]
class DrupalDroplet extends AbstractDroplet implements ComposerExtensionInterface
{

    const TENANT_CLASS = DrupalTenant::class;

    /**
     * @param ExtensionDefinitionInterface $definition
     * @param mixed[]                      $settings
     * @param string                       $appId
     * @param Environment                  $env
     * @param PathHelper                   $pathHelper
     */
    public function __construct(ExtensionDefinitionInterface $definition, array $settings, protected string $appId = 'raini', protected ?Environment $env = null, protected ?PathHelper $pathHelper = null)
    {
        parent::__construct($definition, $settings);
    }

    /**
     * {@inheritdoc}
     */
    public function defaultSettings(): array
    {
        return [
            'cache' => null,
            'mailhog' => true,
            'robotstxt' => true,
            'fileProxy' => false,
            'configSplit' => true,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function createComposerFile(Tenant $tenant): ComposerBuilder
    {
        $data = [
            'name' => $this->appId.'/'.$tenant->getName(),
            'type' => 'project',
            'license' => 'GPL-2.0-or-later',
            'prefer-stable' => true,
            'minimum-stability' => 'dev',
            'config' => [
                'optimize-autoloader' => true,
                'preferred-install' => 'dist',
                'sort-packages' => true,
                'allow-plugins' => [
                    'composer/installers' => true,
                    'drupal/core-composer-scaffold' => true,
                    'oomphinc/composer-installers-extender' => true,
                ],
            ],
            'repositories' => $this->getRepositories(),
            'conflict' => [
                'drupal/drupal' => '*',
            ],
        ];

        return new ComposerBuilder($tenant->getComposerPath(), $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getRepositories(): array
    {
        return [
            'drupal' => [
                'type' => 'composer',
                'url' => 'https://packages.drupal.org/8',
            ],
            'asset-packagist' => [
                'type' => 'composer',
                'url' => 'https://asset-packagist.org',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getPackages(): array
    {
        $packages = [
            'composer/installers',
            'drupal/core-composer-scaffold',
            'drupal/core-recommended',
            'drush/drush',
            'wikimedia/composer-merge-plugin',
        ];

        // Add a caching module for the configured cache service type.
        if ($cacheType = $this->getCacheType()) {
            $packages[] = 'drupal/'.$cacheType;
        }
        // Use the RobotsTXT module provide the robots.txt file contents.
        if ($this->useRobotsTxtModule()) {
            $packages[] = 'drupal/robotstxt';
        }
        // Use stage_file_proxy module for syncing remote files.
        if ($this->useStageFileProxy()) {
            $packages[] = 'drupal/stage_file_proxy';
        }
        // Use config_split and config_ignore modules.
        if ($this->useConfigSplit()) {
            $packages[] = 'drupal/config_ignore';
            $packages[] = 'drupal/config_split';
        }

        return $packages;
    }

    /**
     * {@inheritdoc}
     */
    public function getDevPackages(): array
    {
        return [
            'drupal/core-dev',
            'drupal/config_inspector',
        ];
    }

    /**
     * Get a list of cache types that are supported with our Drupal installs.
     *
     * @return string[] An array listing the supported cache types.
     */
    public static function getAllowedCacheTypes(): array
    {
        return [
            'redis' => 'redis',
            'memcache' => 'memcache',
        ];
    }

    /**
     * Get the cache service to use with this project configuration.
     *
     * @return string|false Get the configure caching implementation to use with this Drupal project (Redis or Memcache)
     *                      This can determine the module caching module to install and changes to the Docker image.
     *                      FALSE is returned if no caching should be use with this project.
     */
    public function getCacheType(): string|false
    {
        $cacheType = $this->settings['cache'] ?: null;

        return $cacheType && isset(static::getAllowedCacheTypes()[$cacheType]) ? $cacheType : false;
    }

    /**
     * Get the cache service for the Drupal tenant.
     *
     * The cache settings will have defaults applied based on the type of the
     * type of cache being utilized and will return NULL if no caching service
     * should be used.
     *
     * @param DrupalTenant $tenant The Drupal tenant to fetch the caching service information for.
     *
     * @return array<string, string|int>|null The caching configurations for the Drupal tenant with defaults applied.
     *                                        When the cache service is unavailable or disabled for an environment the
     *                                        settings are return is NULL.
     */
    public function getCacheInfo(DrupalTenant $tenant): ?array
    {
        $cacheInfo = $tenant->getCacheInfo();

        // Apply the defaults for the cache settings.
        switch ($this->getCacheType()) {
            case 'redis':
                return $cacheInfo + [
                    'host' => 'redis',
                    'port' => '6379',
                    'password' => null,
                ];

            case 'memcache':
                return $cacheInfo + [
                    'host' => 'memcached',
                    'port' => 11211,
                    'servers' => [],
                ];

            case false:
            case null:
                return null;
        }

        return $cacheInfo;
    }

    /**
     * Get the configured default environment to sync to. Can be empty.
     *
     * @return string|null The environment identifier to use as the default synchronize environment.
     */
    public function getDefaultSyncEnvironment(): ?string
    {
        return $this->settings['defaultSync'] ?? null;
    }

    /**
     * @return bool TRUE if the site should be setup to use Drupal robotstxt module.
     */
    public function useRobotsTxtModule(): bool
    {
        return !empty($this->settings['robotstxt']);
    }

    /**
     * @return bool TRUE if the site should use staged file proxy module.
     */
    public function useStageFileProxy(): bool
    {
        return !empty($this->settings['fileProxy']);
    }

    /**
     * Flag to indicate that config_split and config_ignore is used.
     *
     * Projects of course can include and make use of the config_split and
     * config_ignore modules without Raini. This flag allow Raini to be aware
     * of and provide helpful functionality, such as applying config splits
     * after database imports, and database sync processes.
     *
     * @return bool TRUE if Drupal site should utilized, config_split and config_ignore.
     */
    public function useConfigSplit(): bool
    {
        return !empty($this->settings['configSplit']);
    }
}
