<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Devel;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Devel\AnalyzerInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Runs the Drupal PHP code analysis tool on a target.
 *
 * The 'PHPStan' with command is run with a set of rules and code analysis
 * configurations that are tuned specifically for Drupal ("drupal/core-dev"
 * package includes the "mglaman/phpstan-drupal"). The tool does a great job
 * catching bad code references, typing errors and depreciations.
 */
class DrupalAnalyzer implements AnalyzerInterface
{

    /**
     * Constructs a new DrupalAnalyzer service handler instance.
     *
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'Drupal Code Analyzer';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return 'Runs "Drupal check" on Drupal paths.';
    }

    /**
     *{@inheritdoc}
     */
    public function isPathApplicable(PathInfo $path, Tenant $tenant): bool
    {
        $pathType = $path->getType();
        if (in_array($pathType, ['drupal', 'module', 'theme', 'profile'])) {
            return true;
        }

        if (PathInfo::PROJECT_PATH === $pathType) {
            $tenantPath = $tenant->getDocroot();

            if (str_starts_with($path->getFullpath(), $tenantPath.'/')) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(array|PathInfo $path, Tenant $tenant, EnvironmentInterface|ExecutionContextInterface $context, array $options): int
    {
        $cmd = $tenant->getBinDir().'/phpstan';
        $cli = $this->cliFactory->create($cmd, $context);
        $args = ['analyze'];

        // Apply command options for the PHPStan execution.
        if (!empty($options['format'])) {
            $args[] = '--error-format='.$options['format'];
        }
        if (!empty($options['no-progress'])) {
            $args[] = '--no-progress';
        }

        $args[] = '--';

        if (is_array($path)) {
            foreach ($path as $info) {
                $args[] = $info;
            }
        } else {
            $args[] = $path;
        }

        return $cli
            ->setTty(true)
            ->execute($args);
    }
}
