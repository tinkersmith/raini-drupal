<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Devel;

use Raini\Core\Environment;
use Raini\Core\Devel\CodeStandardInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;

/**
 * Adds coding standards and Drupal paths for the CodeStandardManager.
 */
class DrupalCodeStandard implements CodeStandardInterface
{

    /**
     * @param Environment $env
     */
    public function __construct(protected Environment $env)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getInfo(): array
    {
        // "Drupal" and "DrupalPractice" will be autodetected by CodeSniffer
        // and added by the \RainiDev\Devel\CoreStandard coding
        // standard service.
        return [
            'drupalstrict' => ['label' => 'DrupalStrict'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getStandardByPath(PathInfo $pathInfo, Tenant $tenant): ?string
    {
        $pathType = $pathInfo->getType();

        if (in_array($pathType, ['drupal', 'module', 'theme', 'profile'])) {
            return 'drupalstrict';
        }

        if (PathInfo::PROJECT_PATH === $pathType) {
            $tenantPath = $tenant->getDocroot();

            if (str_starts_with($pathInfo->getFullpath(), $tenantPath.'/')) {
                return 'drupalstrict';
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand(Tenant $tenant, array $options = []): string|array
    {
        $cmd = empty($options['fix']) ? 'phpcs' : 'phpcbf';

        return is_readable($tenant->getBasePath()."/$cmd")
            ? $tenant->getBasePath()."/$cmd"
            : $this->env->getBinPath()."/$cmd";
    }

    /**
     * {@inheritdoc}
     */
    public function getStandardValue(string $standard, Tenant $tenant): string
    {
        if ('drupalstrict' !== $standard) {
            throw new \InvalidArgumentException(sprintf('The %s standard is not supported the DrupalCodeStandard handler.', $standard));
        }

        $defFilepath = $tenant->getVendorDir().'/raini/drupal/phpcs.xml';
        if (!file_exists($defFilepath)) {
            $defFilepath = $this->env->getVendorDir().'/raini/drupal/phpcs.xml';
        }

        return $defFilepath;
    }
}
