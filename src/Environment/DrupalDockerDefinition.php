<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Environment;

use Raini\Core\Environment;
use Raini\Core\File\PathHelper;
use Raini\Core\Project\TenantManagerInterface;
use Raini\Core\Utility\EnvValues;
use Raini\Drupal\DrupalDroplet;
use Raini\Drupal\DrupalTenant;

/**
 * Docker container definitions for docker based builders to work from.
 *
 * @todo this is a placeholder for now and needs to be refactored.
 */
class DrupalDockerDefinition
{

    /**
     * @param DrupalDroplet          $droplet
     * @param TenantManagerInterface $tenantManager
     * @param Environment            $env
     * @param PathHelper             $pathHelper
     */
    public function __construct(protected DrupalDroplet $droplet, protected TenantManagerInterface $tenantManager, protected Environment $env, protected PathHelper $pathHelper)
    {
    }

    /**
     * @return mixed[]
     */
    public function containerDefinitions(): array
    {
        $volumes = [];
        $volumes['db'] = [];
        $dbEnvFile = $this->env->getRainiPath().'/sites.db.env';
        $settings = $this->droplet->getSettings();

        $containers['mysql'] = [
            'image' => 'tinkersmith/mariadb:10.6.11',
            'ports' => ['127.0.0.1:3306:3306'],
            'environment' => ['MYSQL_ROOT_PASSWORD=adminadmin'],
            'env_file' => [$dbEnvFile],
            'volumes' => ['db:/var/lib/mysql'],
        ];

        $containers['cli'] = [
            'image' => 'tinkersmith/cli:php8.1',
            'env_file' => [$dbEnvFile],
            'working_dir' => '/var/www/html',
            'volumes' => ['./:/project:shared,rw'],
        ];

        $containers['web'] = [
            'template' => [
                'file' => $this->droplet->getPath().'/templates/web.dockerfile',
                'variables' => [
                    'image' => 'tinkersmith/apache:php8.1-dev',
                    'php_memory' => '128M',
                ],
                'args' => [
                    'SITE_NAME' => 'testkit',
                    'SITE_DOMAIN' => 'local.testkit.com',
                ],
            ],
            'working_dir' => '/var/www/html',
            'ports' => ['127.0.0.1:80:80'],
            'sysctls' => ['net.core.somaxconn=1024'],
            'depends_on' => ['mysql'],
            'env_file' => [$dbEnvFile],
            'volumes' => [],
        ];

        // Add the appropriate caching services to the docker template.
        if ($cacheType = $this->droplet->getCacheType()) {
            $containers['web']['template']['args']['CACHE_SERVICE'] = $cacheType;
        }

        // Add the Mailhog image if option for using Mailhog is present and truthy.
        if (!empty($settings['mailhog'])) {
            $containers['web']['template']['args']['SMTP_HOST'] = 'mailhog:1025';
            $containers['mailhog'] = [
                'image' => 'mailhog/mailhog',
                'ports' => ["127.0.0.1:8025:8025"],
            ];
        }

        $vhosts = [];
        $databases = [];
        $dbEnvValues = file_exists($dbEnvFile) ? EnvValues::createFromFile($dbEnvFile) : new EnvValues();
        $dbEnvValues['MYSQL_USER'] = ($settings['db_user'] ?? 'admin');
        $dbEnvValues['MYSQL_PASSWORD'] = ($settings['db_pass'] ?? 'admin');

        $dropletType = $this->droplet->getName();
        /** @var DrupalTenant $tenant */
        foreach ($this->tenantManager->getTenants($dropletType) as $tenant) {
            $name = $tenant->getName();
            $basePath = $tenant->getBasePath();

            ['base' => $containerPath, 'docroot' => $containerDocroot] = $this->getContainerPath($tenant);

            // Collect the domains that map to this tenant.
            $urls = [];
            foreach ($tenant->getSites() as $site) {
                if ($domain = $site->getDomain()) {
                    if (is_array($domain)) {
                        $urls = array_merge($urls, $domain);
                    } else {
                        $urls[] = $domain;
                    }
                }
            }

            // First domain defines vhost name, and the rest are aliases.
            $url = array_shift($urls);
            $vhosts[] = "vhost create '{$containerDocroot}' $url $name ".implode(',', $urls);
            $vhosts[] = "vhost chown {$containerPath}/storage,{$containerDocroot}/public_files";

            $volumes[$name.'-storage'] = [];
            $volumes[$name.'-public_files'] = [];
            $containers['web']['volumes'][] = "{$basePath}:/var/www/html/{$containerPath}:shared,ro";
            $containers['web']['volumes'][] = "{$name}-storage:/var/www/html/{$containerPath}/storage";
            $containers['web']['volumes'][] = "{$name}-public_files:/var/www/html/{$containerDocroot}/public_files";

            // Capture all the database
            foreach ($tenant->getDatabases() as $dbName => $dbDef) {
                $databases[] = $dbDef['database'];

                // If no user name or password was provided, use the default
                // project DB password.
                if (!empty($dbDef['user']) && !empty($dbDef['pass'])) {
                    $dbEnvValues[$dbName.'_MYSQL_USER'] = $dbDef['user'];
                    $dbEnvValues[$dbName.'_MYSQL_PASSWORD'] = $dbDef['pass'];
                }
            }
        }
        $containers['mysql']['environment'][] = 'MYSQL_DATABASE_LIST='.implode(',', $databases);
        $containers['web']['template']['variables']['default_docroot'] = $containerDocroot ? "'{$containerDocroot}'" : '';
        $containers['web']['template']['variables']['vhost_definitions'] = implode(" && \\\n  ", $vhosts);

        // Write the site database variables to the designated ENV file.
        $dbEnvValues->write($dbEnvFile);

        return [
            'containers' => $containers,
            'volumes' => $volumes,
        ];
    }

    /**
     * Gets the relative file path to the tenant base and docroot directories.
     *
     * These paths are relative to the project root for the "web" and "CLI"
     * docker containers and can be used to find the tenant for commands that
     * run inside containers.
     *
     * @param DrupalTenant $tenant The Drupal tenant to get the container paths for.
     *
     * @return string[] Contains the "base" and "docroot" keys, which point to the tenant base and docroot folder
     *                  respectively.
     */
    public function getContainerPath(DrupalTenant $tenant): array
    {
        $name = $tenant->getName();
        $basePath = $tenant->getBasePath();

        $webPath = './' === $basePath || empty($basePath)
            ? $name : preg_replace('#^\./#', '', $basePath);

        $relPath = $this->pathHelper->makeRelative($tenant->getDocroot(), $basePath);
        $relPath = './' === $relPath || empty($relPath) ? '' : '/'.trim($relPath, '/');

        return [
            'base' => $webPath,
            'docroot' => "{$webPath}{$relPath}",
        ];
    }
}
