<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Composer;

use Composer\Composer;
use Composer\Factory;
use Composer\IO\IOInterface;
use Raini\Core\Composer\InstallerInterface;
use Raini\Core\Extension\ExtensionDefinitionInterface;
use Raini\Core\Settings;
use Raini\Drupal\DrupalDroplet;
use Raini\Drupal\DrupalTenant;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Installation helper to be called when creating a new Drupal Raini project.
 *
 * Installer ensures that the project directory structure is created and Drupal
 * resource files are created (e.g. the hash salt).
 */
class DrupalInstaller implements InstallerInterface
{

    /**
     * Create a new instance of the Drupal Raini project installer.
     *
     * @param Filesystem $fs The class instance for executing filesystem operations.
     */
    public function __construct(protected Filesystem $fs = new Filesystem())
    {
    }

    /**
     * {@inheritdoc}
     */
    public function install(ExtensionDefinitionInterface $extensionDef, Settings $settings, Composer $composer, IOInterface $io): void
    {
        if ($classname = $extensionDef->getClass()) {
            /** @var DrupalDroplet $droplet */
            $droplet = new $classname($extensionDef, $settings->getDropletSettings());
            $isDefault = $droplet->getName() === $settings->getType();
            $tenants = $this->buildTenants($droplet, $settings->getTenants(), $isDefault);

            foreach ($tenants as $tenant) {
                $composerFile = $tenant->getComposerPath();
                $composer = Factory::create($io, $composerFile, true, true);

                $packageRepo = $composer
                    ->getRepositoryManager()
                    ->getLocalRepository();

                // Create the basic folder structure if not already present.
                if ($packageRepo->findPackages('drupal/core') || $packageRepo->findPackages('drupal/core-recommended')) {
                    $dirs = $this->getProjectDirs($composer);
                    $this->createFiles($dirs);
                } else {
                    $io->error('Drupal core package is missing. Project composer file should have either "drupal/core" or "drupal/core-recommended".');
                }
            }
        }
    }

    /**
     * Get the base web and project directories for the Drupal installation.
     *
     * @param Composer $composer The root directory of this project or install. Should where composer is run from.
     *
     * @return string[] The array with keys "web-root" and "project-root"
     */
    protected function getProjectDirs(Composer $composer): array
    {
        try {
            $extraConfig = $composer
                ->getPackage()
                ->getExtra();

            $dirs = $extraConfig['drupal-scaffold']['locations'] ?? [];
        } catch (\Exception $e) {
            $dirs = [];
        }

        // Ensure that the root web directory exists.
        if (empty($dirs['web-root'])) {
            $dirs['web-root'] = file_exists('web/core') ? 'web/' : './';
        }
        if (empty($dirs['project-root'])) {
            $dirs['project-root'] = ($dirs['web-root'] === './') ? './' : "../{$dirs['web-root']}";
        }

        $dirs['web-root'] = rtrim($dirs['web-root'], '/\ ');
        $dirs['project-root'] = rtrim($dirs['project-root'], '/\ ');

        return $dirs;
    }

    /**
     * Create file locations used by the Drupal website.
     *
     * @param string[] $dirs The base project and web root directories.
     */
    protected function createFiles(array $dirs): void
    {
        // Create additional project directories if they are not already present.
        $mkDirs = [
            "{$dirs['web-root']}/libraries",
            "{$dirs['web-root']}/profiles",
            "{$dirs['web-root']}/modules",
            "{$dirs['web-root']}/themes",
        ];

        foreach ($mkDirs as $dir) {
            if (!$this->fs->exists($dir)) {
                $this->fs->mkDir($dir);
            }
        }
    }

    /**
     * Build all the Drupal tenants currently defined.
     *
     * @param DrupalDroplet $droplet           The Drupal Droplet to build the tenant for.
     * @param mixed[]       $tenantDefinitions All the tenants defined in the raini.project.yml file.
     * @param bool          $isDefault         Is this Drupal droplet the active/default project droplet type.
     *
     * @return DrupalTenant[] List DrupalTenant instances.
     */
    protected function buildTenants(DrupalDroplet $droplet, array $tenantDefinitions, bool $isDefault): array
    {
        /** @var DrupalTenant[] $tenants */
        $tenants = [];
        foreach ($tenantDefinitions as $name => $def) {
            $type = @$def['type'] ?: null;
            if ((!$type && $isDefault) || $droplet->getName() === $type) {
                /** @var DrupalTenant $tenant */
                $tenant = $droplet->buildTenant($name, $def);
                $tenants[$name] = $tenant;
            }
        }

        return $tenants;
    }
}
