<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\File\PathInfo;
use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrupalTenant;
use Tinkersmith\Console\CliInterface;
use Tinkersmith\Console\ExecutionContextInterface;
use Tinkersmith\Console\Formatter\NullFormatter;
use Tinkersmith\Console\Output\BufferedOutput;
use Tinkersmith\Environment\EnvironmentInterface;

/**
 * A trait which can be added to classes that need to call Drush from the CLI.
 *
 * This allows commands and other utilities to call Drush use the same settings
 * and handle environment contexts consistently. Environments can have a 'drush'
 * property which can explicitly set a "docroot" or URI pattern to use when
 * running a Drush command on that environment.
 */
trait DrushCliTrait
{

    /**
     * The CLI command factory service.
     *
     * @var CliFactoryInterface
     */
    protected CliFactoryInterface $cliFactory;

    /**
     * Get a Cli command object to run Drush commands for a target site.
     *
     * @param DrupalSite                                     $site    The Drupal site to run the Drush command on.
     * @param EnvironmentInterface|ExecutionContextInterface $context The environment context to run the command in.
     *
     * @return CliInterface The Cli command object to run Drush commands on a target environment and site.
     */
    protected function buildDrushCmd(DrupalSite $site, EnvironmentInterface|ExecutionContextInterface $context): CliInterface
    {
        /** @var EnvironmentInterface $environment */
        $environment = ($context instanceof ExecutionContextInterface) ? $context->getEnvironment() : $context;

        /** @var DrupalTenant $tenant */
        $tenant = $site->getTenant();

        // Prefer the version with PHP extension, but fallback if needed.
        $drushPath = new PathInfo(PathInfo::PROJECT_PATH, $tenant->getBinDir().'/drush.php');
        if (!is_readable($drushPath)) {
            $drushPath = new PathInfo(PathInfo::PROJECT_PATH, $tenant->getBinDir().'/drush');
        }
        $cmd = [$drushPath];

        // Set the site environment being targeted.
        $envConf = $environment->getDefinition()['drush'] ?? null;
        $cmd[] = '-r';
        $cmd[] = @$envConf['docroot'] ?: new PathInfo(PathInfo::PROJECT_PATH, $tenant->getDocroot());

        if (!empty($envConf['uri'])) {
            $tokens = [
              'site' => $site->getName(),
              'tenant' => $tenant->getName(),
              'env' => $environment->id(),
            ];

            $cmd[] = '-l';
            $cmd[] = preg_replace_callback('#\{([a-z0-1_\- ]+)\}#i', function (array $matches) use ($tokens) {
                return $tokens[$matches[1]] ?? '';
            }, $envConf['uri']);
        } elseif ($tenant->isMultiSite()) {
            $domain = $site->getDomain();
            $cmd[] = '-l';
            $cmd[] = is_array($domain) ? reset($domain): $domain;
        }

        return $this->cliFactory->create($cmd, $context);
    }

    /**
     * Check if a Drupal module is enabled.
     *
     * @param string                                         $name    The name of the module.
     * @param DrupalSite                                     $site    The Drupal site to check for the module.
     * @param ExecutionContextInterface|EnvironmentInterface $context The Drush execution context.
     *
     * @return bool TRUE IFF the module is enabled for the target site.
     */
    protected function isModuleEnabled(string $name, DrupalSite $site, ExecutionContextInterface|EnvironmentInterface $context): bool
    {
        // If search API is enabled, refresh the search indexes.
        $buffer = new BufferedOutput(formatter: new NullFormatter());
        $this->buildDrushCmd($site, $context)->execute([
            'pml',
            '--no-core',
            '--status=enabled',
            '--type=module',
            '--field=name',
            "--filter={$name}",
        ], $buffer);

        return $name === trim($buffer->fetch());
    }
}
