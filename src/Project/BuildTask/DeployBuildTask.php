<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Project\BuildTask;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Project\BuildOptions;
use Raini\Core\Project\BuildTaskInterface;
use Raini\Core\Project\Exception\BuildTaskIncompleteException;
use Raini\Core\Project\Exception\SiteNotFoundException;
use Raini\Core\Project\Tenant;
use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrupalTenant;
use Raini\Drupal\DrushCliTrait;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Executes Drush functions for a Drupal deployment.
 *
 * Ensures that the Drupal application is up to date with the installed database
 * updates, have applied the latest configurations from the sync file and
 * deploy hooks have all been run.
 *
 * This is normally the process for deployments to hosted environments or after
 * a development environment has been updated with Drupal module updates, module
 * additions and configuration updates.
 */
class DeployBuildTask implements BuildTaskInterface
{

    use DrushCliTrait;

    /**
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceId(): string
    {
        return 'drupal_deploy';
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return 'Drupal deployment';
    }

    /**
     * {@inheritdoc}
     */
    public function enabled(Tenant $tenant): bool
    {
        // Only run this build task for Drupal tenants.
        return $tenant instanceof DrupalTenant;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Tenant $tenant, BuildOptions $options, ?OutputInterface $output = null): void
    {
        /** @var DrupalTenant $tenant */
        $sites = $tenant->getSites();

        if (!$tenant->isMultiSite()) {
            $sites = ['default' => reset($sites)];
        } elseif ($options->targetSite) {
            if (!$options->targetSite instanceof DrupalSite) {
                $msg = sprintf('Site identified by "%s" was found but is not a valid Drupal site.', $options->targetSite->getName());
                throw new SiteNotFoundException($msg);
            }

            // Reduce the sites list to only the target site definition.
            $site = $options->targetSite;
            $sites = [$site->getName() => $site];
        }

        try {
            /** @var DrupalSite $site */
            foreach ($sites as $site) {
                if ($output) {
                    $msg = sprintf('Deploying <info>%s</> site', $site->getName());
                    $output instanceof SymfonyStyle ? $output->section($msg) : $output->writeln($msg);
                }

                $drushCmd = $this->buildDrushCmd($site, $options->environment);
                try {
                    $drushCmd->setTty(false);
                    $drushCmd->execute(['cr'], null);
                } catch (\InvalidArgumentException|RuntimeException) {
                    // The first Drush cache rebuild can fail, but it is okay.
                    // It can happen if a required Drupal database update is
                    // required, but we need still need to run cache rebuild to
                    // flush the module, bootstrap and discovery caches.
                    // So cache flush is needed and it is expected that
                    // sometimes it can fail.
                }

                // Perform deployment operations for this site in the
                // recommended Drupal order.
                $drushCmd->setTty(true);
                $drushCmd->execute(['updb', '-y'], $output);
                $drushCmd->execute(['cim', '-y'], $output);
                $drushCmd->execute(['cr'], $output);
                $drushCmd->execute(['deploy:hook', '-y'], $output);

                if ($output) {
                    $output->writeln('');
                }
            }
        } catch (RuntimeException|CliRuntimeException $e) {
            if ($output) {
                $output->writeLn("<error>".$e->getMessage()."</>");
            }

            throw new BuildTaskIncompleteException();
        }
    }
}
