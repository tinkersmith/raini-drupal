<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Utility;

use Raini\Core\Project\Tenant;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;
use Symfony\Component\Finder\Finder;

/**
 * Finds a Drupal module, theme or profile within a Drupal site installation.
 */
class DrupalPackageLocator
{

    /**
     * The site project base path.
     *
     * @var string
     */
    protected string $basepath;

    /**
     * The web docroot where Drupal is installed.
     *
     * @var string
     */
    protected string $docroot;

    /**
     * Constructs a new DrupalPackageLocator utility instance.
     *
     * @param Tenant $tenant Provides the base search directories to look for Drupal resources in.
     */
    public function __construct(Tenant $tenant)
    {
        $this->basepath = $tenant->getBasePath();
        $this->docroot = $tenant->getDocroot();
    }

    /**
     * Get an array of directories to search for Drupal extensions of $type.
     *
     * @param string $type The type of Drupal resource being searched for. Should be one of
     *                     "module", "theme" or "profile".
     *
     * @return string[] A list of directories to search for Drupal resources of the provided $type.
     */
    public function getDirectories(string $type): array
    {
        $dirs = [
            $this->docroot."/core/{$type}s/**/",
            $this->docroot."/{$type}s/**/",
            $this->docroot."/sites/*/{$type}s/**/",
        ];

        if ('module' === $type || 'theme' === $type) {
            $dirs[] = $this->docroot."/profiles/**/{$type}s/**/";
        } elseif ('profile' !== $type) {
            // If not "module", "theme" or "profile" it is an unsupported type.
            throw new \InvalidArgumentException();
        }

        return $dirs;
    }

    /**
     * @param string $type     The type of resource ("module", "theme", or "profile") to search for.
     * @param string $resource Name of the module, theme or profile to search for.
     *
     * @return string|null The path to the module, theme or profile if a matching the resource was found.
     *                     NULL otherwise. The path is relative to the project directory.
     */
    public function find(string $type, string $resource): ?string
    {
        $finder = new Finder();
        $finder
            ->files()
            ->depth('< 5')
            ->ignoreVCS(true)
            ->ignoreDotFiles(true)
            ->ignoreUnreadableDirs(true)
            ->exclude([
                'assets',
                'config',
                'css',
                'js',
                'src',
                'includes',
                'templates',
                'node_modules',
            ]);

        $hasDir = false;
        foreach ($this->getDirectories($type) as $dir) {
            try {
                $finder->in($dir);
                $hasDir = true;
            } catch (DirectoryNotFoundException $e) {
                continue;
            }
        }

        // Look for a valid info file for the module, theme or profile.
        $finder->name("$resource.info.yml");

        return $hasDir && $finder->hasResults()
            ? $finder->getIterator()->current()->getPath()
            : null;
    }
}
