<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\Utility;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Utility to help with creating Hash Salt values and hash secrets file.
 */
class SaltGenerator
{

    /**
     * @param int  $size         The number of bytes for the size of the results.
     * @param bool $returnBinary Should the results be a binary string. If false, a Base64 string is returned.
     *
     * @return string The hash salt value. If $returnBinary is TRUE, the result is a binary byte string with the value
     *                and if the $returnBinrary param is FALSE, then return a Base64 string.
     */
    public static function createSalt(int $size = 64, bool $returnBinary = false): string
    {
        $bytes = null;
        $isSecure = false;

        if (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($size, $isSecure);
        }

        if (empty($bytes || !$isSecure)) {
            try {
                $bytes = random_bytes($size);
            } catch (\Exception $e) {
                list($usec, $sec) = explode(' ', microtime());
                mt_srand(intval($sec) + intval($usec) * 1000000);

                $bytes = '';
                for ($i = 0; $i < ($size >> 2); ++$i) {
                    $bytes .= pack('N', mt_rand());
                }
            }
        }

        return $returnBinary ? $bytes : base64_encode($bytes);
    }

    /**
     * @param string $filepath  The filepath to write the hashsalt data to.
     * @param int    $size      The number of bytes for the size of the results.
     * @param bool   $useBinary Should the results be a binary string. If false, a Base64 string is returned.
     */
    public static function createSaltFile(string $filepath, int $size = 64, bool $useBinary = false): void
    {
        $fs = new Filesystem();
        $salt = static::createSalt($size, $useBinary);

        // Create the new salt file using a random generated value.
        $fs->dumpFile($filepath, $salt);
    }
}
