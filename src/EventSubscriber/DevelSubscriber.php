<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\EventSubscriber;

use Raini\Dev\Event\AnalyzeSettingsAlterEvent;
use Raini\Dev\Event\DevGeneratorEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The event subscriber for raini-dev configurations.
 *
 * @see static::getSubscribedEvents()
 */
class DevelSubscriber implements EventSubscriberInterface
{

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            DevGeneratorEvents::PHP_ANALYZE_SETTINGS_ALTER => [
                ['onAnalyzeSettingsAlter', 0],
            ],
        ];
    }

    /**
     * Alter the PHPStan settings to apply Drupal project settings.
     *
     * @param AnalyzeSettingsAlterEvent $event The analyze settings event.
     */
    public function onAnalyzeSettingsAlter(AnalyzeSettingsAlterEvent $event): void
    {
        $tenant = $event->getTenant();
        $docroot = $tenant->getDocroot();
        $settings = &$event->getAnalyzeSettings();

        $drupalBaseline = "{$docroot}/core/.phpstan-baseline.php";
        if (false === array_search($drupalBaseline, $settings['includes']) && file_exists($drupalBaseline)) {
            $settings['includes'][] = "{$docroot}/core/.phpstan-baseline.php";
        }

        // Drupal coding standards don't require the specification of array
        // types. If high than 5, PHPStan will throw errors for array typehints
        // that are valid for Drupal standards.
        if ($settings['parameters']['level'] > 5) {
            $settings['parameters']['level'] = 5;
        }

        // Add Drupal specific parameters.
        $settings['parameters'] += [
            'excludePaths' => [],
            'ignoreErrors' => [],
            'reportUnmatchedIgnoredErrors' => false,
            'treatPhpDocTypesAsCertain' =>  false,
        ];

        $settings['parameters']['excludePaths'] = array_unique(array_merge($settings['parameters']['excludePaths'] ?? [], [
            '*/tests/fixtures/*.php',
            '**/*/templates/*',
        ]));

        if (false === array_search("#^Unsafe usage of new static#", $settings['parameters']['ignoreErrors'])) {
            $settings['parameters']['ignoreErrors'][] = "#^Unsafe usage of new static#";
        }
    }
}
