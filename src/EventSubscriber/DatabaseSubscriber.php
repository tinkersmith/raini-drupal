<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\EventSubscriber;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Event\Database\AlterDbCommandEvent;
use Raini\Core\Event\Database\DatabaseEvents;
use Raini\Core\Event\Database\DbImportEvent;
use Raini\Core\Event\Database\StructureTableEvent;
use Raini\Drupal\DrupalDroplet;
use Raini\Drupal\DrushCliTrait;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;
use Tinkersmith\Console\Formatter\NullFormatter;
use Tinkersmith\Console\Output\BufferedOutput;

/**
 * The Raini event subscriber to listen for Raini database events.
 *
 * @see static::getSubscribedEvents()
 */
class DatabaseSubscriber extends AbstractDatabaseSubscriber
{

    use DrushCliTrait;

    /**
     * @param DrupalDroplet       $droplet          The Drupal droplet instance and settings.
     * @param CliFactoryInterface $cliFactory       The CLI factory.
     * @param string[]            $structOnlyTables List Drupal database table names to export as structure with no data.
     * @param string[]            $backupOnlyTables List Drupal database table names to only include data when doing full backups.
     *                                              This means that the data for these tables will not be including with regular
     *                                              database syncs.
     */
    public function __construct(protected DrupalDroplet $droplet, protected CliFactoryInterface $cliFactory, protected array $structOnlyTables = [], protected array $backupOnlyTables = [])
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            DatabaseEvents::ALTER_COMMAND => [
                ['onAlterDbCommand', 0],
            ],
            DatabaseEvents::STRUCTURE_TABLE_REGEX => [
                ['onDbExportStructureTables', 0],
            ],
            DatabaseEvents::POST_IMPORT => [
                ['onDbPostImport', 0],
            ],
        ];
    }

    /**
     * Alter the options and parameters for Symfony Database commands.
     *
     * @param AlterDbCommandEvent $event The database command alter event.
     *
     * @see DatabaseEvents::ALTER_COMMAND
     */
    public function onAlterDbCommand(AlterDbCommandEvent $event): void
    {
        if (in_array($event->name, ['drupal:sync', 'db:import']) && $this->droplet->useConfigSplit()) {
            $event
                ->getCommand()
                ->addOption('config-split', null, InputOption::VALUE_NEGATABLE|InputOption::VALUE_NONE, 'Apply active config-splits to the local environment after database import completes.');
        }
    }

    /**
     * Add Drupal structure only table patterns if the default Drupal table.
     *
     * Only the default database (and its replicas) are used as the main Drupal
     * database and has the tables we wish to ignore. Other tables (at least
     * for now) should export all their data.
     *
     * @param StructureTableEvent $event The event object for updating structure table patterns.
     *
     * @see DatabaseEvents::STRUCTURE_TABLE_REGEX
     */
    public function onDbExportStructureTables(StructureTableEvent $event): void
    {
        $targetDb = $event->getTargetDb();

        if ($this->isDrupalDatabase($event->getDbProvider(), $targetDb)) {
            $event->addTableRegex([
                'batch',
                'cache(tags|_.+)',
                'search(|_.+)',
                'semaphore',
                'sessions',
            ]);

            if ($this->structOnlyTables) {
                $event->addTableRegex($this->structOnlyTables);
            }

            // When not used as a backup, also exclude these data tables.
            if (!$event->isBackup) {
                // When not creating backups, these tables are structure only.
                // This normally for content that could be private and the data
                // shouldn't be exported.
                $event->addTableRegex($this->backupOnlyTables);
            }
        }
    }

    /**
     * Execute Drupal post import scripts.
     *
     * At a minimum the scripts will ensure caches are refreshed and the
     * Drupal database updates are applied. This is the minimum to ensure a
     * working Drupal instance.
     *
     * @param DbImportEvent $event The database import settings, target and execution context.
     */
    public function onDbPostImport(DbImportEvent $event): void
    {
        // Only execute deploy scripts if the Drupal database was imported.
        if ($site = $this->getDatabaseSite($event->getDbProvider(), $event->getTargetDb())) {
            $context = $event->getContext() ?? $this->cliFactory->getDefaultContext();
            $options = $event->getOptions();
            $drushCmd = $this->buildDrushCmd($site, $context);
            $output = $event->getOutput();

            /** @var DrupalDroplet $droplet */
            $droplet = $site->getTenant()->getDroplet();

            try {
                $drushCmd->setTty(false);
                $drushCmd->execute(['cr'], null);
            } catch (\InvalidArgumentException|RuntimeException) {
                // The first Drush cache rebuild can fail, but it is okay.
            }

            // The alway run "updatedb" to ensure db schema is up to date.
            $drushCmd->setTty(true);
            $drushCmd->execute(['updb', '-y'], $output);

            // These operations are optional, normally we should run them but.
            if ($event->useScripts()) {
                $drushCmd->execute(['cim', '-y'], $output);
                $drushCmd->execute(['cr'], $output);
                $drushCmd->execute(['deploy:hook', '-y'], $output);

                // Only execute if the Search API module is enabled.
                if ($this->isModuleEnabled('search_api', $site, $context)) {
                    $drushCmd->execute(['sapi-c']);
                    $drushCmd->execute(['sapi-i'], $output);
                }
            } elseif ($droplet->useConfigSplit() && $options['config-split'] && $this->isModuleEnabled('config_split', $site, $context)) {
                $buffer = new BufferedOutput(formatter: new NullFormatter());
                $cmd = $this->buildDrushCmd($site, $context);
                $cmd->execute(['cst', '--format=list', '--state=Any', '--prefix=config_split'], $buffer);

                $uninstall = [];

                // Look for and execute active config_splits.
                // @phpstan-ignore-next-line argument.type
                foreach (array_filter(explode("\n", $buffer->fetch()), strlen(...)) as $split) {
                    try {
                        $cmd->execute(['cget', '--include-overridden', '--format=php',  $split], $buffer);
                        $config = @unserialize($buffer->fetch(), ['allowed_classes' => false]) ?: [];

                        if (!empty($config['status'])) {
                            if ($output) {
                                $output->writeln("Apply local config-split: <info>{$split}</> for <info>".$site->getName().'</>');
                            }

                            $drushCmd->execute(['config-split:import', '-y', $split], $output);
                        } elseif (!empty($config['module'])) {
                            $uninstall += $config['module'];
                        }
                    } catch (\InvalidArgumentException|RuntimeException) {
                        // Thrown usually if the split doesn't exist, it's okay to skip.
                    }
                }

                // Uninstall modules from disabled config_splits.
                if ($uninstall) {
                    try {
                        $drushCmd->setTty(true);
                        $drushCmd->execute(['pmu', '-y', ...array_keys($uninstall)]);
                    } catch (CliRuntimeException) {
                    }
                }
            }
        }
    }
}
