<?php

/*
 * This file is part of the Raini Drupal package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Drupal\EventSubscriber;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\Project\DatabaseProviderInterface;
use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrupalTenant;
use Raini\Drupal\DrushCliTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Output\BufferedOutput;

/**
 * The Raini event subscriber to listen for Raini events.
 *
 * @see static::getSubscribedEvents()
 */
abstract class AbstractDatabaseSubscriber implements EventSubscriberInterface
{

    use DrushCliTrait;

    /**
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * @param DatabaseProviderInterface $provider The site or tenant that owns this database.
     * @param string                    $targetDb The target database name.
     *
     * @return bool TRUE if the target database is a Drupal structured database.
     */
    protected function isDrupalDatabase(DatabaseProviderInterface $provider, string $targetDb): bool
    {
        return (bool) $this->getDatabaseSite($provider, $targetDb);
    }

    /**
     * Get the Drupal site  that the target database is core database for.
     *
     * Find the Drupal site that the target database is the default Drupal
     * database for. This subscribed events only execute for the Drupal system
     * database is being imported or exported.
     *
     * @param DatabaseProviderInterface $provider The database provider.
     * @param string                    $targetDb The target database being imported.
     *
     * @return DrupalSite|null The Drupal site that the target database is the main Drupal database for. If we cannot
     *                         find a site that the database belongs to, or it is not the default database for,
     *                         return NULL.
     */
    protected function getDatabaseSite(DatabaseProviderInterface $provider, string $targetDb): ?DrupalSite
    {
        if ($provider instanceof DrupalSite && $provider->isDrupalDatabase($targetDb)) {
            return $provider;
        }

        if ($provider instanceof DrupalTenant) {
            /** @var DrupalSite $site */
            foreach ($provider->getSites() as $site) {
                // Only the default Drupal databases have the tables filtered by this
                // event, and so detect if this is the database with the Drupal
                // structure (the site's default database).
                if ($site->isDrupalDatabase($targetDb)) {
                    return $site;
                }
            }
        }

        return null;
    }

    /**
     * Uninstall "complete split" modules from a config_split.
     *
     * Can only reliably remove modules that are specified for a complete split
     * because partial splits could have configurations that belong to the split
     * and belong to the common configuraitons. This isn't easy to determine
     * cleanly which configurations should be kept or are being overridden
     * without doing a configuration import.
     *
     * @param DrupalSite           $site        The site to target for removing the config split modules.
     * @param EnvironmentInterface $environment The target environment to uninstall
     * @param string[]             $splitIds    The list of config split IDs to uninstall modules for.
     */
    protected function removeSplitModules(DrupalSite $site, EnvironmentInterface $environment, array $splitIds): void
    {
        $modules = [];
        $buffer = new BufferedOutput();
        $drushCmd = $this->buildDrushCmd($site, $environment);

        foreach ($splitIds as $splitId) {
            try {
                $drushCmd->execute(['cget', '--include-overridden', '--format=php',  "config_split.config_split.{$splitId}"], $buffer);
                $config = @unserialize($buffer->fetch(), ['allowed_classes' => false]) ?: [];

                if (!empty($config['module'])) {
                    $modules += $config['module'];
                }
            } catch (\InvalidArgumentException|RuntimeException) {
                // Thrown usually if the split doesn't exist, it's okay to skip.
            }
        }

        if ($modules) {
            $drushCmd->setTty(true);
            $drushCmd->execute(['pmu', '-y', ...array_keys($modules)]);
        }
    }
}
