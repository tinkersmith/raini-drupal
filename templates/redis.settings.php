<?php

// phpcs:ignoreFile

use Composer\Autoload\ClassLoader;
use Drupal\Core\Installer\InstallerKernel;

/**
 * The path to the Redis contrib module.
 *
 * Alter this path if your Redis module is installed in a different location.
 */
$redis_path = "$app_root/modules/contrib/redis";

/**
 * Ensure that the requirements for using the Redis cache are available before
 * configuring the Redis services and backends.
 */
if (!InstallerKernel::installationAttempted() && class_exists('Redis') && file_exists($redis_path . '/redis.services.yml')) {
  if (!isset($class_loader)) {
    $class_loader = new ClassLoader();
  }
  $class_loader->addPsr4('Drupal\\redis\\', $redis_path .'/src');
  $class_loader->register();

  $settings['container_yamls'][] = $redis_path . '/redis.services.yml';

  // Prefer services defined (in order of preference):
  //  - site path directory
  //  - default site path
  //  - Redis module definitions
  //
  // First available is used service definition file is used.
  if (file_exists("$app_root/$site_path/redis.services.yml")) {
    $settings['container_yamls'][] = "$app_root/$site_path/redis.services.yml";
  }
  elseif (file_exists("$app_root/default/redis.services.yml")) {
    $settings['container_yamls'][] = "$app_root/default/redis.services.yml";
  }

  // Set the Redis Host and cache information. For specific environment settings
  // these values may need to be overridden in an environment specific settings.
  $settings['redis.connection']['interface'] = 'PhpRedis';
  $settings['redis.connection']['host'] = 'redis';
  $settings['redis.connection']['port'] = '6379';
  $settings['redis.connection']['password'] = NULL;
  $settings['redis.connection']['cache_prefix'] = FALSE;

  // Performance improvement from https://www.drupal.org/node/3500807.
  $settings['redis_invalidate_all_as_delete'] = TRUE;

  // Use for all queues unless otherwise overridden explicitly.
  $settings['queue_default'] = 'queue.redis'; // 'queue.redis_reliable';

  // Default caching.
  $settings['cache']['default'] = 'cache.backend.redis';

  // Ensure that these caches are set to the Redis backend instead of the
  // fast chained backend for multi-node environment. This ensures that the
  // cache flushes aren't limited to the APCu cache on an individual node.
  $settings['cache']['bins']['bootstrap'] = 'cache.backend.redis';
  $settings['cache']['bins']['discovery'] = 'cache.backend.redis';
  $settings['cache']['bins']['config'] = 'cache.backend.redis';

  // Use redis for container cache.
  // The container cache is used to load the container definition itself, and
  // provides the cache definitions needed to utilize Redis during the bootstrap
  // process rather than having to bootstrap with SQL caches.
  //
  // Be aware that this requires you to clear the Redis cache when running a
  // re-running a Drupal install, since the DB might be empty but the Redis
  // cache will still be providing stale configurations from a previous install.
  $settings['bootstrap_container_definition'] = [
    'parameters' => [],
    'services' => [
      'redis.factory' => [
        'class' => 'Drupal\redis\ClientFactory',
      ],
      'cache.backend.redis' => [
        'class' => 'Drupal\redis\Cache\CacheBackendFactory',
        'arguments' => [
          '@redis.factory',
          '@cache_tags_provider.container',
          '@serialization.phpserialize',
        ],
      ],
      'cache.container' => [
        'class' => 'Drupal\redis\Cache\PhpRedis',
        'factory' => ['@cache.backend.redis', 'get'],
        'arguments' => ['container'],
      ],
      'cache_tags_provider.container' => [
        'class' => 'Drupal\redis\Cache\RedisCacheTagsChecksum',
        'arguments' => ['@redis.factory'],
      ],
      'serialization.phpserialize' => [
        'class' => 'Drupal\Component\Serialization\PhpSerialize',
      ],
    ],
  ];
}
