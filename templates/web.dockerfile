FROM {{ image }}

ARG SITE_NAME=Drupal
ARG SITE_DOMAIN=localhost
ARG CACHE_SERVICE
ARG SMTP_HOST

# Setup up the site VHosts and PHP configuration overrides.
RUN /scripts/setup.sh setHostname ${SITE_DOMAIN} {{ default_docroot }} && \
  {{ vhost_definitions }}

RUN config php --multi \
  'memory_limit' '{{ php_memory }}' \
  'max_execution_time' '60'

# Install additional services that are optional for specific Drupal
RUN if [[ ! -z "${CACHE_SERVICE}" ]]; then \
  /scripts/setup.sh configure ${CACHE_SERVICE}; \
fi

# If an SMTP server is specified for sending and consuming the emails apply the
# SSMTP forwarding configurations. In most cases this would be a Mailhog docker
# container with the build-arg "SMTP_MAIL_HOST" value set to "mailhost:1025".
RUN if [[ ! -z "${SMTP_HOST}" ]]; then \
  apk add --update --no-cache ssmtp && \
  config ini 'mailhub' "${SMTP_HOST}" /etc/ssmtp/ssmtp.conf && \
  config php 'sendmail_path' "/usr/sbin/sendmail -S ${SMTP_HOST}"; \
fi

# ========================================
# Add customizations below this line
# ========================================

# Dockerfile additions and changes from here on are preserved when project
# is generated or Dockerfiles are updated.

# Enhanced debugging options (only 1st line is required):
# ------------- Uncomment items below line as needed ----------------- #
# RUN config php --multi --file=xdebug \
#   'xdebug.default_enable' '1' \
#   'xdebug.collect_params' '4' \
#   'xdebug.collect_vars' 'on' \
#   'xdebug.dump_globals' 'on' \
#   'xdebug.show_local_vars' 'on' \
#   'xdebug.max_nesting_level' '1000' \
#   'xdebug.show_mem_delta' '1'
