<?php

// phpcs:ignoreFile

use Composer\Autoload\ClassLoader;
use Drupal\Core\Installer\InstallerKernel;

/**
 * The path to the Memcache contrib module.
 *
 * Alter this path if your memcache module is installed in a different location.
 */
$memcache_path = "$app_root/modules/contrib/memcache";

/**
 * Ensure that the requirements for using the memcache cache are available
 * before configuring the memcache services and backends.
 */
if (!InstallerKernel::installationAttempted() && file_exists($memcache_path . '/memcache.services.yml')) {
  $hasMemcached = class_exists('Memcached', FALSE);

  if (class_exists(ClassLoader::class) && ($hasMemcached || class_exists('Memcache', FALSE)) ) {
    $class_loader = new ClassLoader();
    $class_loader->addPsr4('Drupal\\memcache\\', $memcache_path .'/src');
    $class_loader->register();

    // Use Memcached extension over memcache when possible.
    if ($hasMemcached) {
      $settings['memcache']['extension'] = 'Memcached';
      $settings['memcache']['options'][\Memcached::OPT_TCP_NODELAY] = TRUE;

      // If IGBINARY serializer is available, prefer it.
      if (\Memcached::HAVE_IGBINARY) {
        $settings['memcache']['options'][\Memcached::OPT_SERIALIZER] = \Memcached::SERIALIZER_IGBINARY;
      }
    }

    $settings['container_yamls'][] = $memcache_path . '/memcache.services.yml';

    // Prefer services definitions (in order of preference):
    //  - site path directory
    //  - default site path
    //  - Memcache module definitions
    //
    // First available is used service definition file is used.
    if (file_exists("$app_root/$site_path/memcache.services.yml")) {
      $settings['container_yamls'][] = "$app_root/$site_path/memcache.services.yml";
    }
    elseif (file_exists("$app_root/default/memcache.services.yml")) {
      $settings['container_yamls'][] = "$app_root/default/memcache.services.yml";
    }

    // Set the Redis Host and cache information. For specific environment settings
    // these values may need to be overridden in an environment specific settings.
    $settings['memcache']['servers'] = ["memcached:11211" => 'default'];
    $settings['memcache']['bins'] = ['default' => 'default'];
    $settings['memcache']['key_prefix'] = '' . basename($site_path);

    // Default caching.
    $settings['cache']['default'] = 'cache.backend.memcache';

    // Ensure that these caches are set to the memcache backend instead of the
    // fast chained backend for multi-node environment. This ensures that the
    // cache flushes aren't limited to the APCu cache on an individual node.
    $settings['cache']['bins']['bootstrap'] = 'cache.backend.memcache';
    $settings['cache']['bins']['discovery'] = 'cache.backend.memcache';
    $settings['cache']['bins']['config'] = 'cache.backend.memcache';

    // Use memcache for the container cache.
    // The container cache is used to load the container definition itself, and
    // provides the cache definitions needed to utilize memcache during the
    // bootstrap process rather than having to bootstrap with SQL caches.
    //
    // Be aware that this requires you to clear the memcache cache when running a
    // re-running a Drupal install, since the DB might be empty but the memcache
    // cache will still be providing stale configurations from a previous install.
    $settings['bootstrap_container_definition'] = [
      'parameters' => [],
      'services' => [
        'database' => [
          'class' => 'Drupal\Core\Database\Connection',
          'factory' => 'Drupal\Core\Database\Database::getConnection',
          'arguments' => ['default'],
        ],
        'settings' => [
          'class' => 'Drupal\Core\Site\Settings',
          'factory' => 'Drupal\Core\Site\Settings::getInstance',
        ],
        'memcache.settings' => [
          'class' => 'Drupal\memcache\MemcacheSettings',
          'arguments' => ['@settings'],
        ],
        'memcache.factory' => [
          'class' => 'Drupal\memcache\Driver\MemcacheDriverFactory',
          'arguments' => ['@memcache.settings'],
        ],
        'cache_tags_provider.container' => [
          'class' => 'Drupal\Core\Cache\DatabaseCacheTagsChecksum',
          'arguments' => ['@database'],
        ],
        'memcache.timestamp.invalidator.bin' => [
          'class' => 'Drupal\memcache\Invalidator\MemcacheTimestampInvalidator',
          'arguments' => [
            '@memcache.factory',
            'memcache_bin_timestamps',
            0.001,
          ],
        ],
        'memcache.backend.cache.container' => [
          'class' => 'Drupal\memcache\DrupalMemcacheInterface',
          'factory' => ['@memcache.factory', 'get'],
          'arguments' => ['container'],
        ],
        'cache.container' => [
          'class' => '\Drupal\memcache\MemcacheBackend',
          'arguments' => [
            'container',
            '@memcache.backend.cache.container',
            '@cache_tags_provider.container',
            '@memcache.timestamp.invalidator.bin',
            '@memcache.settings',
          ],
        ],
      ],
    ];
  }
}
